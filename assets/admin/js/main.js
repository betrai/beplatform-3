$(document).ready(function() {
    
   $('#data').DataTable({
	    'language'		: {'url':'//cdn.datatables.net/plug-ins/1.10.16/i18n/Vietnamese.json'},
	    'paging'      	: true
	});
	
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    });
    			
	$(".currency").each(function(index){
		//console.log(this.nodeName);
		if(this.nodeName == "INPUT"){
			$(this).val(currency($(this).val(),"đ"));
		}else{
			$(this).text(currency($(this).text(),"đ"));
		}
	});
	
	
	/*Ajax form*/
	$('#ajax_submit').on('submit',function(){
		url = $(this).attr('action');
		dataString = $(this).serialize();
		
		$.ajax({
			type: "POST",
			url: url,
			data:dataString,
			success: function(data){
				if(data.success){
					$('#modal-success').modal('show');
					$('#print').show();
					$('#generate_pdf').show();
					$('button[name="submit_invoice"]').hide();
					
				}else{
					$('#modal-danger').modal('show');

				}
			}
		})
		
		return false;
	})
	
	$('button[name=submit_invoice]').click(function(){
		alert('abc');
		$('#ajax_submit').submit();
		return false;
	});
	
	$('button.back').on("click",function(){
		window.history.back();
	});
	
	$('.modal').modal('show');
		
});

function removecommas(val){
	return val.toString().replace(/,/g,'');
}