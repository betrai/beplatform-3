(function($) {
    
    "use strict";

    // ==================== Preloader
    function preloader_load() {
        if($('.preloader').length){
            $('.preloader').delay(300).fadeOut(500);
        }
    }

    // ==================== Menuzord Megamenu
    function menuzord() {
        if ($('.menuzord').length) {
            $('.menuzord').menuzord({
            align: "left",
            effect: "none",
            animation: "zoom-out",
            indicatorFirstLevel: "<i class='fa fa-angle-down'></i>",
            indicatorSecondLevel: "<i class='fa fa-angle-right'></i>"
            });
        }
    }

    // ==================== Progress Bar / Levels
    if($('.progress-levels .progress-box .bar-fill').length){
        $(".progress-box .bar-fill").each(function() {
            var progressWidth = $(this).attr('data-percent');
            $(this).css('width',progressWidth+'%');
            $(this).children('.percent').html(progressWidth+'%');
        });
    }

    // ==================== PieChart     
    if($('#chart, #chart2, #chart3, #chart4').length){
        $("#chart").circliful({
            animation: 1,
            animationStep: 5,
            foregroundBorderWidth: 6,
            backgroundBorderWidth: 6,
            percent: 90,
            textSize: 28,
            textStyle: 'font-size: 18px;',
            textColor: '#ffffff',
            multiPercentage: 1,
            percentages: [10, 20, 30]
        });
        $("#chart2").circliful({
            animation: 1,
            animationStep: 5,
            foregroundBorderWidth: 6,
            backgroundBorderWidth: 6,
            percent: 75,
            textSize: 28,
            textStyle: 'font-size: 18px;',
            textColor: '#ffffff',
            multiPercentage: 1,
            percentages: [10, 20, 30]
        });
        $("#chart3").circliful({
            animation: 1,
            animationStep: 5,
            foregroundBorderWidth: 6,
            backgroundBorderWidth: 6,
            percent: 70,
            textSize: 28,
            textStyle: 'font-size: 18px;',
            textColor: '#ffffff',
            multiPercentage: 1,
            percentages: [10, 20, 30]
        });
        $("#chart4").circliful({
            animation: 1,
            animationStep: 5,
            foregroundBorderWidth: 6,
            backgroundBorderWidth: 6,
            percent: 90,
            textSize: 28,
            textStyle: 'font-size: 18px;',
            textColor: '#ffffff',
            multiPercentage: 1,
            percentages: [10, 20, 30]
        });
    }

    // ==================== Navbar Scroll To Fixed
    $(document).ready(function() {
        $('.scrollingto-fixed').scrollToFixed();
        var summaries = $('.summary');
        summaries.each(function(i) {
            var summary = $(summaries[i]);
            var next = summaries[i + 1];
            summary.scrollToFixed({
                marginTop: $('.scrollingto-fixed').outerHeight(true) + 10,
                limit: function() {
                    var limit = 0;
                    if (next) {
                        limit = $(next).offset().top - $(this).outerHeight(true) - 10;
                    } else {
                        limit = $('.footer').offset().top - $(this).outerHeight(true) - 10;
                    }
                    return limit;
                },
                zIndex: 999
            });
        });
    });

    // ====================Prelader
    function preloader() {
        var $preloader = $('#preloader');
        $preloader.delay(200).fadeOut('slow');
    }

    // ==================== Gallery Masonry Isotop And Other Gallery and Lightbox
    function masonryIsotop() {
        if ($('.masonry-gallery').length) {
            $('.masonry-gallery').isotope({
                layoutMode:'masonry'
            });
        }
        if($('.masonry-filter').length){
            $('.masonry-filter a').children('span').click(function(){
                var Self = $(this);
                var selector = Self.parent().attr('data-filter');
                $('.masonry-filter a').children('span').parent().removeClass('active');
                Self.parent().addClass('active');
                $('.masonry-gallery').isotope({ filter: selector });
            });
    }
        
    /*================================ magnificPopup  ================================*/
    /* http://dimsemenov.com/plugins/magnific-popup/ // view-source:http://dimsemenov.com/plugins/magnific-popup/ */

    // lightbox image
    $(document).ready(function() {
        $('.popup-gallery').magnificPopup({
          delegate: 'a',
          type: 'image',
          tLoading: 'Loading image #%curr%...',
          mainClass: 'mfp-img-mobile',
          gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
          },
          image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
              return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
            }
          }
        });
        
        /**/
		$('img.crop-img-news').jQcrop({
			width: 750,
			height:357,
		}).on('crop.jQcrop', function(e, data){
			console.log('coordinates: ' +data);
		});
	/**/
    });

    $(document).ready(function() {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        });
    });


    //LighvtBox / Fancybox
    if($('.lightbox-image').length) {
      $('.lightbox-image').fancybox();
    }

    }


    // ==================== Paralex Backgrounds
    $.stellar({
       horizontalScrolling: false,
       responsive: true
    });

    // ==================== Tab
    function widgetTab() {
        if ($('.tab-widget').length) {
            var tabWrap = $('.tab-widget-content');
            var tabClicker = $('.tab-widget ul li');            
            tabWrap.children('div').hide();
            tabWrap.children('div').eq(0).show();
            tabClicker.on('click', function() {
                var tabName = $(this).data('tab-name');
                tabClicker.removeClass('active');
                $(this).addClass('active');
                var id = '#'+ tabName;
                tabWrap.children('div').not(id).hide();
                tabWrap.children('div'+id).fadeIn('500');
                return false;
            });        
        }
    }

    // ==================== Wow animation
    function wowAnimation() {
        var wow = new WOW({
            mobile: true // trigger animations on mobile devices (default is true)
        });
        wow.init();
    }

    // ==================== fitVids
    function fit_Vids() {
        $body.fitVids();
    }

    // ==================== YTplayer
    if($('.player').length) {
        $(".player").mb_YTPlayer();
    }

    // ==================== Accordions
    if($('.accordion-box').length){
       $('.accordion-box .acc-btn').click(function() {
        if($(this).hasClass('active')!==true){
                $('.accordion-box .acc-btn').removeClass('active');
            }
            
        if ($(this).next('.acc-content').is(':visible')){
                $(this).removeClass('active');
                $(this).next('.acc-content').slideUp(500);
            }
        else{
                $(this).addClass('active');
                $('.accordion-box .acc-content').slideUp(500);
                $(this).next('.acc-content').slideDown(500);    
            }
        });
    }

    // ==================== OWL CAROUSEL AND OTHER SLIDER SCRIPT 
    // Owl-News-carousel
    if($('.owl-carousel-grid-one, .blog-img-carousel').length){
        $('.owl-carousel-grid-one').owlCarousel({
            loop:true,
            margin:30,
            dots: false,
            nav:true,
            autoplayHoverPause:false,
            autoplay: false,
            smartSpeed: 700,
            navText: [
              '<i class="lnr lnr-arrow-left"></i>',
              '<i class="lnr lnr-arrow-right"></i>'
            ],
            responsive: {
                0: {
                    items: 1,
                    center: false
                },
                480:{
                    items:1,
                    center: false
                },
                600: {
                    items: 1,
                    center: false
                },
                768: {
                    items: 1
                },
                992: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        })
    }

    if($('.testimonial-carousel').length){
        $('.testimonial-carousel').owlCarousel({
            loop:true,
            margin:30,
            dots: false,
            nav:false,
            autoplayHoverPause:false,
            autoplay: true,
            smartSpeed: 700,
            navText: [
              '<i class="lnr lnr-arrow-left"></i>',
              '<i class="lnr lnr-arrow-right"></i>'
            ],
            responsive: {
                0: {
                    items: 1,
                    center: false
                },
                480:{
                    items:1,
                    center: false
                },
                600: {
                    items: 1,
                    center: false
                },
                768: {
                    items: 1
                },
                992: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        })
    }

    // Owl-News-carousel
    if($('.owl-carousel-grid-two, .news-slider').length){
        $('.owl-carousel-grid-two, .news-slider').owlCarousel({
            loop:true,
            margin:30,
            dots: false,
            nav:true,
            autoplayHoverPause:false,
            autoplay: true,
            smartSpeed: 700,
            navText: [
              '<i class="lnr lnr-arrow-left"></i>',
              '<i class="lnr lnr-arrow-right"></i>'
            ],
            responsive: {
                0: {
                    items: 1,
                    center: false
                },
                480:{
                    items:1,
                    center: false
                },
                600: {
                    items: 1,
                    center: false
                },
                768: {
                    items: 1
                },
                992: {
                    items: 2
                },
                1200: {
                    items: 2
                }
            }
        })
    }

    // Owl-gallery-carousel
    if($('.owl-carousel-grid-three, .gallery-slider').length){
        $('.owl-carousel-grid-three, .gallery-slider').owlCarousel({
            loop:true,
            margin:10,
            dots: false,
            nav:true,
            autoplayHoverPause:false,
            autoplay: false,
            smartSpeed: 700,
            navText: [
              '<i class="lnr lnr-arrow-left"></i>',
              '<i class="lnr lnr-arrow-right"></i>'
            ],
            responsive: {
                0: {
                    items: 1,
                    center: false
                },
                480:{
                    items: 1,
                    center: false
                },
                600: {
                    items: 2,
                    center: false
                },
                768: {
                    items: 2,
                    center: false
                },
                992: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
        })
    }

    // ==================== Scroll To top
    function scrollToUped() {
        $(window).scroll(function(){
            if ($(this).scrollTop() > 600) {
                $('.scrollToUp').fadeIn();
            } else {
                $('.scrollToUp').fadeOut();
            }
        });
        
        //Click event to scroll to top
        $('.scrollToUp').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
    }


/* ==========================================================================
   When document is ready, do
   ========================================================================== */
    $(document).on('ready', function() {
        // add your functions
        menuzord();
        scrollToUped();
        wowAnimation();
        widgetTab();
    });

/* ==========================================================================
   When document is Scrollig, do
   ========================================================================== */
    // window on Scroll function
    $(window).on('scroll', function() {
        // add your functions
    });
    
/* ==========================================================================
   When document is loading, do
   ========================================================================== */

    $(window).on('load', function() {
        // add your functions
        preloader_load();
        masonryIsotop();
    }); 


/* ==========================================================================
   When Window is resizing, do
   ========================================================================== */
    $(window).on('resize', function() {
        // add your functions
    });
    
    
    $('.active').bind('click',function(){
	    
    });
    
    



})(window.jQuery);
