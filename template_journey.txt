Array
(
    [student_id] => 7
    [type_deflation] => percent
    [show_deflation_amount] => 20
    [customer_name] => Trần Thế Anh
    [customer_address] => 443/23
    [customer_phone] => 443/23
    [customer_email] => 
    [customer_province_name] => Bà Rịa - Vũng Tàu
    [customer_ward_name] => Huyện Hoàng Su Phì
    [pay_amount] => 20000000
    [total_fee_after_deflation] => 21000000
    [deflation_amount] => 
    [total_fee] => 21000000
    [items] => Array
        (
            [1] => Array
                (
                    [name] => Học phí
                    [price] => 15000000
                    [deflation_amount] => 
                )

            [2] => Array
                (
                    [name] => Tiền hóa chất
                    [price] => 3000000
                )

            [3] => Array
                (
                    [name] => Tiền dụng cụ
                    [price] => 3000000
                )

        )

    [invoice_no] => 1
    [invoice_type] => HV
    [invoice_date] => 21-03-2018 09:00:50
)