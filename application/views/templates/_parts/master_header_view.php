<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="<?=$current_lang['language_code']?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php 
	$title = $Settings['company_name'];
	
	if(isset($page_title)){
		echo $page_title .":". $title;
	}else if(isset($item->translation[0]->page_title)){
		echo $item->translation[0]->page_title.": ".$title;
	}else{
		echo $title;
	}
?></title>
<?php
	/*Meta keywords and description*/
	$keywords = $Settings['keywords'];
	$description = $Settings['description'];
	if(isset($item->translations[0]->page_keywords)){
		$keywords = $item->translations[0]->page_keywords;
	}
	
	if(isset($item->translations[0]->page_description)){
		$description = $item->translations[0]->page_description;
	}
		
echo "
<!-- meta tag -->
<meta name='keywords' content='".$keywords."' />
<meta name='description' content='".$description."' />";
echo "
<!-- Facebook -->
<!-- meta og:title -->
<meta property='og:title' content='".$title."' />
    
<!-- meta og:description -->
<meta property='og:description' content='".$description."' />
	 ";  
	    
	?>
<!-- Latest compiled and minified CSS -->		
<link rel="icon" href="<?=base_url()?>assets/img/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo site_url('assets/css/style.css');?>">
<!-- css file -->
<link href="<?php echo site_url('assets/css/menuzord-border-bottom.css')?>" rel="stylesheet"/>
<!-- Responsive stylesheet -->
<link rel="stylesheet" href="<?php echo site_url('assets/css/responsive.css')?>">
<!-- REVOLUTION LAYERS STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/revolution-slider/css/settings.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/revolution-slider/css/layers.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/revolution-slider/css/navigation.css')?>">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php 
	echo $css_for_elements;
?>
</head>
<body>
		<!-- Main Header Start -->
	<header>
		<div class="my-header-top-bar">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-2">
						<div class="my-top-bar-col my-contact-num">
							<ul class="my-list-inline">
								<li><a href="#"><i class="lnr lnr-phone"></i><?=$Settings['company_phone_1']?></a></li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-7">
						<div class="my-top-bar-col my-address pull-left">
							<ul class="my-list-inline">
								<li><a href="#"><i class="lnr lnr-map-marker"></i><?=$Settings['address']?> </a></li>
							</ul>
						</div>
						<div class="my-top-bar-col">
						    <ul class="my-list-inline my-font-icons">
							    <?php
									if(!empty($Settings['social_facebook'])):
								?>
						        <li><a href="<?=$Settings['social_facebook']?>"> <i class="fa fa-facebook"></i></a></li>
						        <?php
							        endif;
							        
							        if(!empty($Settings['social_linkedin'])):
							    ?>
						        <li><a href="<?=$Settings['social_linkedin']?>"> <i class="fa fa-linkedin"></i></a></li>
						        <?php
							        endif;
							        ?>
						    </ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<div class="my-top-bar-col pull-right">
							<?php
								if(!empty($langs)):
								?>
							<ul class="my-list-inline my-lang-cart">
								<li class="my-border-right-two">
								
									<div class="dropdown">
					                    <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" id="dropdownMenu1" type="button" class="btn my-default-btn my-lang dropdown-toggle">
					                    
					                      <i class="fa fa-globe"></i> <?=__('Languages',$this)?> <span class="caret"></span>
					                    </button>
					                    <ul aria-labelledby="dropdownMenu1" class="dropdown-menu">
						                    <?php
							                    foreach($langs as $k =>$l):
							                ?>
							                    
					                        <li><a href="<?=base_url().$l->slug?>"><i><img alt="" src="<?=base_url()?>/assets/img/resource/<?=$l->image?>"></i> <?=__($l->language_name,$this)?></a></li> 
					                        <?php
						                    	endforeach;
						                    ?>
					                    </ul>
					                </div>
								</li>
								<!--<li><a class="my-carts-icon" href="#"><i class="lnr lnr-cart"></i> <sup>2</sup></a></li>-->	
								
							</ul>
							<?php
								endif;
								?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header Navigator -->
		<?php $this->load->view('elements/header_navigator')?>
		
		<!-- End Heaver Nav -->
	</header>
	<!-- Main Header end -->
	

	