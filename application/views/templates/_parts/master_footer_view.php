
	<!-- Footer Start -->
    <footer class="my-footer">
    	<div class="container">
    		<div class="row">
	    		<!-- Col 1 -->
    			<?php $this->load->view('elements/footer/footer_corporate_view')?>
    			<!-- Col 2 -->
    			<?php $this->load->view('elements/footer/lastest_news_view')?>
    			<!-- Col 3 -->
    			<?php $this->load->view('elements/footer/useful_links_view')?>
				<!-- Col 4 -->
    			<?php $this->load->view('elements/footer/news_letter_view')?>
    		</div>
    	</div>
    </footer>
    <!-- Footer Blog End  -->



	<a class="scrollToUp" href="#"><i class="fa fa-angle-up"></i></a>

	<!-- script start from here -->
	<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/jquery-ui.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/scrollto.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.knob.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/modernizr.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/jquery-scrolltofixed-min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/menuzord.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.masonry.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/stellar.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/wow.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/owl.carousel.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/jquery.magnific-popup.min.js');?>"></script>
	<script src="<?=base_url()?>assets/js/jquery.googlemap.js"></script>
	<!-- REVOLUTION JS FILES -->
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/jquery.themepunch.tools.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/jquery.themepunch.revolution.min.js');?>"></script>
	<!-- Custom script for all pages --> 
	<script type="text/javascript" src="<?php echo site_url('assets/js/script.js');?>"></script>

	<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
			(Load Extensions only on Local File Systems ! 
			 The following part can be removed on Server for On Demand Loading) -->
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.actions.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.carousel.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.kenburn.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.migration.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.navigation.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.parallax.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.slideanims.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/revolution-slider/js/extensions/revolution.extension.video.min.js');?>"></script>

	<!-- END REVOLUTION SLIDER -->
	<script type="text/javascript">
	    var tpj = jQuery;
	    var revapi202;
	    tpj(document).ready(function() {
	        if (tpj("#rev_slider1").revolution == undefined) {
	            revslider_showDoubleJqueryError("#rev_slider1");
	        } else {
	            revapi202 = tpj("#rev_slider1").show().revolution({
	                sliderType: "standard",
	                sliderLayout: "fullscreen",
	                dottedlayer: "none",
	                delay: 9000,
	                navigation: {
	                    keyboardNavigation: "off",
	                    keyboard_direction: "horizontal",
	                    mouseScrollNavigation: "off",
	                    onHoverStop: "off",
	                    touch: {
	                        touchenabled: "on",
	                        swipe_threshold: 75,
	                        swipe_min_touches: 50,
	                        swipe_direction: "horizontal",
	                        drag_block_vertical: false
	                    },
	                    bullets: {
	                        enable: true,
	                        hide_onmobile: true,
	                        hide_under: 800,
	                        style: "zeus",
	                        hide_onleave: false,
	                        direction: "horizontal",
	                        h_align: "center",
	                        v_align: "bottom",
	                        h_offset: 0,
	                        v_offset: 30,
	                        space: 5,
	                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imagelayer"></span><span class="tp-bullet-title">{{title}}</span>'
	                    }
	                },
	                responsiveLevels: [1240, 1024, 778, 480],
	                visibilityLevels: [1240, 1024, 778, 480],
	                gridwidth: [1240, 1024, 778, 480],
	                gridheight: [868, 768, 960, 720],
	                lazyType: "none",
	                parallax: {
	                    type: "scroll",
	                    origo: "slidercenter",
	                    speed: 1000,
	                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
	                    type: "scroll",
	                },
	                shadow: 0,
	                spinner: "off",
	                stopLoop: "on",
	                stopAfterLoops: 0,
	                stopAtSlide: 1,
	                shuffle: "off",
	                autoHeight: "off",
	                fullScreenAutoWidth: "off",
	                fullScreenAlignForce: "off",
	                fullScreenOffsetContainer: "",
	                fullScreenOffset: "60px",
	                disableProgressBar: "on",
	                hideThumbsOnMobile: "off",
	                hideSliderAtLimit: 0,
	                hideCaptionAtLimit: 0,
	                hideAllCaptionAtLilmit: 0,
	                debugMode: false,
	                fallbacks: {
	                    simplifyAll: "off",
	                    nextSlideOnWindowFocus: "off",
	                    disableFocusListener: false,
	                }
	            });
	        }
	    }); /*ready*/
	</script>
	<!-- Slider Revolution Start -->

	<!-- FUNFACT SCRIPT START -->
	<script type="text/javascript">
     $(document).ready(function($) {
          $('.start-count').each(function() {
                  var $this   = $(this);
                  $this.data('target', parseInt($this.html()));
                  $this.data('counted', false);
                  $this.html('0');
              });
              
              $(window).bind('scroll', function() {
                  var speed   = 3000;
                  $('.start-count').each(function() {
                      var $this   = $(this);
                      if(!$this.data('counted') && $(window).scrollTop() + $(window).height() >= $this.offset().top) {
                          $this.data('counted', true);
                          $this.animate({dummy: 1}, {
                              duration: speed,
                              step:     function(now) {
                                  var $this   = $(this);
                                  var val     = Math.round($this.data('target') * now);
                                  $this.html(val);
                                  if(0 < $this.parent('.value').length) {
                                      $this.parent('.value').css('width', val + '%');
                                  }
                              }
                          });
                      }
                  });
              }).triggerHandler('scroll');
      });
	</script>
	
	<?php
		/*
			Google Analytics
		*/
		if(!empty($Settings['google_analytics_id'])):
	?>
	<!-- Google Analytics id: <?=$Settings['google_analytics_id']?> -->
	<script type='text/javascript'>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
		ga('create', '<?=$Settings['google_analytics_id']?>', 'auto');
		ga('send', 'pageview');
	</script>
	<!-- End Google Analytics id -->
	<?php
		endif;	
	?>
	
	<!-- -->
	<script>
		document.addEventListener("contextmenu", function(e){
		    e.preventDefault();
		}, false);
	</script>	
	<?php echo $before_body;?>
</body>

</html>