<?php 
	$this->load->view('elements/page_header_view',$item);
?>
<section class="my-inner-blog-field my-about-two">
	<div class="container">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="my-sector-topp">
					<span class="icon-toolbox"></span>
	                <h2><span><?=$item->translations[0]->name?></span></h2>
	                <p><?=$item->translations[0]->content?></p>
				</div>
			</div>
		
		<div class='row'>
			<div class='col-sm-3'>
				<?php $this->load->view('elements/recruitments_left_view',$items);
				?>
			</div>
			<div class='col-sm-9'>
				<?php
					foreach($items as $k=>$v):
					?>
					<div class='my-about-col editor-content'>
					<h4>
						
						<span style='color:#0074b6;'><?=$v->translations[0]->name?></span></h4>
						<span class='date'><?=date('d M Y',strtotime($v->created_at))?></span>
						<p></p>
						<?=$v->translations[0]->content?>
						<p></p>
						<?php
							if(!empty($v->translations[0]->file)):
						?>
						<p><strong><span><?=_("Download Form: ",$this)?></span></strong> <a href='<?=base_url()?>assets/upload/file/<?=$v->translations[0]->file?>'><?=$v->translations[0]->file?></a></p>
						<?php
							endif;
							?>
					</div>
					<hr style='background:#0074b6;width:100%' />
				<?php
					endforeach;
					?>
			</div>
		</div>

	</div>
</section>