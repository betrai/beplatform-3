<?php 
	$this->load->view('elements/page_header_view',$item);
?>
<section class="my-inner-blog-field my-about-two">
	<div class="container">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="my-sector-topp">
				<span class="icon-compass"></span>
                <h2><span><?=$item->translations[0]->teaser?></span></h2>
                <p><?=$item->translations[0]->content?></p>
			</div>
		</div>
		<div class='row'>
			<div class='col-sx-12 col-sm-4 col-md-4'>
				<?php $this->load->view('elements/capability_left_view',$items);
				?>
			</div>
			<div class='col-sx-12 col-sm-8 col-md-8'>
				<?php
					foreach($items['departments'] as $k=>$v):
					?>
					<div class='my-about-col'>
					<h4>
						<span style='color: #0074b6;'><?=$v->translations[0]->name?></span></h4>
						<img src='<?=base_url()?>assets/upload/img/<?=$v->content_image?>' class='img-responsive' /><br />
						<p><?=$v->translations[0]->content?></p>
					</div>
				<?php
					endforeach;
					?>
			</div>
		</div>
	</div>
</section>