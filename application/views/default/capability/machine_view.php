<?php 
	$this->load->view('elements/page_header_view',$item);
?>
<section class="my-inner-blog-field my-about-two">
	<div class="container">
			<div class="col-sm-10 col-sm-offset-1">
						<div class="my-sector-topp">
							<span class="icon-compass"></span>
			                <h2><span><?=$item->translations[0]->teaser?></span></h2>
			                <p><?=$item->translations[0]->content?></p>
						</div>
					</div>
		
		<div class='row'>
			<div class='col-sm-3 col-md-3 col-lg-3'>
				<?php $this->load->view('elements/capability_left_view',$items);
				?>
			</div>
			<div class='col-sm-9 col-md-9 col-lg-9'>
				<p style='font-size:24px;font-weight:bold;'><?=__('We have well equitment from Japan, Most of all are new',$this)?></p>
				<div style='margin-bottom:50px;'></div>
				<?php
					$i = 1;
					foreach($items['machines'] as $k=>$v):
					?>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                    <div class="about-img-column">
	                        <img class="img-responsive my-img-fluided" src="<?=base_url()?>assets/upload/img/<?=$v->image?>" alt="<?=$v->translations[0]->name?>">
	                        <div class="layer">
	                            <div class="img-col-content">
	                              <!--<h4 class="title"><?=$v->translations[0]->name?></h4>-->
	                              <p><?=$v->translations[0]->name?></p>
	                            </div>
	                        </div>
	                    </div>
					</div>
				<?php
						if($i == 2){
							echo "<div class='clearfix'></div>";
							$i = 1;
						}else{
							$i ++;
						}
					endforeach;
					?>
			</div>
		</div>
	</div>

</section>