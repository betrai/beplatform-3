<?php 
	$this->load->view('elements/page_header_view',$item);
	?>
<section class="my-about-field my-about-two">
	<div class="container">
		<div class='row'>
			<div class='col-sm-4'>
				<h3><span class='brand_color'><?=$item->translations[0]->name?></span></h3>
				<div>
					<?=$item->translations[0]->content?>
				</div>
			</div>
			<div class='col-sm-8'>
				<video width="100%" height="" controls>
				  <source src="<?=base_url()?>assets/upload/video/<?=$intro->file?>" type="video/mp4">
				  Your browser does not support the video tag.
				</video>
			</div>
		</div>
	</div>
</section>
<section class='my-sector-topp'>
	<div class="container">
		<div class='row'>
			<?php
				if(!empty($items)):
					foreach($items as $k => $v):
				?>
			<div class='col-xs-12 col-sm-6 col-md-3'>
				<a href='<?=base_url()?>assets/upload/video/<?=$v->file?>?iframe=true' rel="prettyPhoto" title="<?=$v->translations[0]->title?>"><img src="<?=base_url()?>assets/upload/video/<?=$v->thumbnail?>" class='img-responsive' /></a>
				<p><?=$v->name?></p>
			</div>
			<?php
					endforeach;
				endif;
				?>
		</div>
	</div>
</section>