<div class="box-header <?=isset($border)?"with-border":""?>">
  <h3 class="box-title"><?=__($box_title,$this)?></h3>
  <?php
	  if(isset($box_tool)):
	  ?>
  <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
       </div>
   <?php
	   endif;
	  ?>
</div>