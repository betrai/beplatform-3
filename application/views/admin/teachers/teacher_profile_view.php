<?php 
	echo form_open('admin/teachers/edit/'.$data->id);
	
	echo form_hidden('password','thaotay123');
	echo form_hidden('password_confirm','thaotay123');
	
	echo form_hidden('company',"THAO TAY");
	echo form_hidden('user_id',$data->id);
	echo form_hidden('redirect',base_url(uri_string()));
	
	echo form_hidden('relation_model[many_to_many]',"teachers_courses");

?>
	<!-- Student Information -->
		<div class='box box-primary box-border'>
			<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>'Teacher Information','box_tool'=>true,'border'=>true))?>
			
			<div class='box-body'>
				<div class='row'>
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						<!-- User name -->	
				          <div class="form-group">
				            <label for='d_of_consulting'><?=__('User Name',$this)?>:</label>
							<input name="username" type="text" class="form-control" id="username" placeholder="<?=__("User name",$this)?>" value='<?=$data->username?>' readonly>
	
				            <!-- /.input group -->
				          </div>
						
				        <!-- Last name -->	
				          <div class="form-group">
				            <label for='d_of_consulting'><?=__('Last Name',$this)?>:</label>
							<input name="last_name" type="text" class="form-control" id="last_name" placeholder="<?=__("Last name",$this)?>" value='<?=$data->last_name?>' >
	
				            <!-- /.input group -->
				          </div>
				          
							<!-- First name -->	
				          <div class="form-group">
				            <label for='d_of_consulting'><?=__('First Name',$this)?>:</label>
							<input name="first_name" type="text" class="form-control" id="frst_name" placeholder="<?=__("First name",$this)?>" value='<?=$data->first_name?>' >
	
				            <!-- /.input group -->
				          </div>
				          
				          <!-- Course Class -->
				          <!-- /.form-group -->
			              <div class="form-group">
			                <label><?=__('Course',$this)?></label>
			                <select name='course_class' class="form-control select2"  style="width: 100%;">
				                <?php
					                foreach($courses as $k => $v):
					                ?>
										<option value='<?=$k?>' <?=$data->current_class==$k?"selected":""?>><?=$v?></option>
			                  <?php
				                  	endforeach;
				                  ?>
			                </select>
			              </div>
					</div>
					
					<!-- -->
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						<!-- Email -->
						<div class='form-group'>
							<label><?=__("Email",$this)?></label>
					        <input name="email" type="text" class="form-control" id="email" placeholder="<?=__("Email",$this)?>" value='<?=$data->email?>' >
						</div>
						
						<!-- Mobile Phone -->
						<div class='form-group'>
							<label><?=__("Phone",$this)?></label>
					        <input name="phone" type="text" class="form-control" id="phone" placeholder="<?=__("Phone",$this)?>" value='<?=$data->phone?>'>
						</div>
						
						<!-- /.form-group -->
			              <div class="form-group">
			                <label><?=__('Group',$this)?></label>
			                <select name='groups[]' class="form-control select2"  style="width: 100%;">
			                  <option selected="selected" value='5'><?=__("Teachers",$this)?></option>
			                </select>
			              </div>
						
					</div>
				</div>	         
			</div>
			
			<!-- ./end box-body -->
		    <div class="box-footer">
			    <button type="submit" class="btn btn-primary"><?=__('Submit',$this)?></button>
			 </div>

		</div>
	<!-- ./end student information -->
	


<?php 
echo form_close();
?>