<div class='row'>   
	<div class='col-md-12'>

	<div class="box">
	   	<!-- /.box-header -->
		<div class="box-header">
		  <h3 class="box-title"><?=__($page_name,$this)?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table id="data" class="table table-bordered table-striped table-hover">
		    <thead>
		    <tr>
		      <th class='text-center'><?=__("Name",$this)?></th>
		      <th class='text-center'><?=__("Phone",$this)?></th>
		      <th class='text-center'><?=__("Current Class",$this)?></th> 
		      <th class='text-center'><?=__("Status",$this)?></th>  
		      <th class='text-center'><?=__("Actions",$this)?></th>             
		    </tr>
		    </thead>
		    <tbody>
	        <?php
	            foreach($data as $k => $v):
	            ?>
		    <tr>
		      <td><a href="<?=base_url()?>admin/teachers/profile/<?=$v->id?>"><?=$v->first_name." ".$v->last_name?></a></td>
		      <td class='text-right'><?=$v->phone?></td>
		      <td class='text-right'>
			      <!--<?=__('Course',$this).' '.current($v->courses)->name. ' - '. __('Class',$this).' '. current($v->classes)->name?>-->
		      </td>
		      <td class='text-center'>
			      <?=active($v->active,$v->id,array('class'=>'minimal-red active','data-url'=>base_url().'admin/processing/ajax_updateField/user/active/'.$v->id,'data-active'=>$v->active))?>
			    </td>
			  <td class='text-right'>
			  	<!-- Trash item -->
			  	<?=anchor('admin/teachers/profile/'.$v->id,"<i class='fa fa-pencil-square-o'></i>")?>
			  	&nbsp;
			  	<?=anchor('admin/teachers/unactive/'.$v->id,"<i class='fa fa-trash'></i>")?>
			  </td> 
		    </tr>
		    <?php
		        endforeach;
		        ?>
		    </tbody>
		    <tfoot>
		        <tr>
			      <th class='text-center'><?=__("Name",$this)?></th>
			      <th class='text-center'><?=__("Phone",$this)?></th>
			      <th class='text-center'><?=__("Current Class",$this)?></th> 
			      <th class='text-center'><?=__("Status",$this)?></th>  
			      <th class='text-center'><?=__("Actions",$this)?></th>             
			    </tr>
		    </tfoot>
		  </table>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
	</div>
       

<?php 
	$this->load->view('admin/teachers/add_teacher');
?>
	</div>
</div>

<?php
	/*Message box*/
	//$this->load->view('admin/elements/modules/message_box_view',array('title'=>__("Info",$this),'message'=>__("Status has been change",$this)));
?>