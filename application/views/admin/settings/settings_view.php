<!-- Main Content -->
<div class='row'>
<div class='col-right col-md-12 col-sm-12'>
		<form class="form-horizontal" action="<?=site_url('admin/settings/submit')?>" method="post">
			<div class='box box-default'>
				<div class="box-header with-border">
		          <h3 class="box-title"><?=__(strtoupper($action),$this)?></h3>
		
		          <div class="box-tools pull-right">
		            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		          </div>
		        </div>
		    
		    <div class='box-body'>
			<?php 	
				foreach($form_settings as $k => $v):
			?>	
				<div class="form-group">
			      <label class="control-label col-sm-3" for="pwd"><?=$v->name?></label>
			     	 <div class="col-sm-8"> 
			     	 	<?php 
				     	 	
				     	 	switch($v->form_type){
					     	 	case "textarea":
					    ?>
					     	 		<textarea class="form-control editor" id="<?=$v->name?>" name="<?=$v->form_name?>"><?=$v->value?></textarea>
					 	<?php
					     	 		break;
					     	 	case "image":
					     	 		echo '<p style="padding-bottom:10px;"><img src="'.base_url().'assets/upload/img/thumbnail/'.$v->value.'" width=100/></p>';
					     	 		//echo "<input type='hidden' name='".$v->form_name."[]' value=".$v->value." />";
					    			$this->load->view('admin/elements/fileupload_view.php',array('file'=>$v->form_name,'id'=>$v->form_name,'value'=>$v->value,'multiple'=>false));
					     	 		break;
					     	 	case "checkbox":
					     	 		echo "<input name='".$v->form_name."'  id='".$v->form_name."' type='hidden' ".($v->value=="Y" ? 'checked' : '')." value='N'/>";
					     	 		echo form_checkbox($v->form_name,'Y',($v->value=='Y'?true:false));
					     	 		break;
					     	 	case "select":
					     	 		echo form_dropdown($v->form_name,$this->config->item($v->config_variable),$v->value,'class="form-control"');
					     	 		break;
					     	 	default:
					     	 		echo form_input($v->form_name,$v->value,'class="form-control"');	
					     	 	
				     	 	}
				     	?>
			     	 		
			    	</div>
			    </div>
			<?php 
				
				endforeach;
			?>
		    
				<div class="form-group">        
			      <div class="col-sm-offset-3 col-sm-10">
			        <button type="submit" class="btn btn-primary">Submit</button>
			      </div>
		    	</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
