<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container">
	<div class="col-sm-3 col-md-3">
		<?php $this->load->view('admin/elements/left_menu_view');?>
	</div>
	<div class="col-right col-sm-9 col-md-9">
		<h3 class="text-capitalize"><?php echo $page_name ?></h3>
		<!-- Languages Selection -->
		<?php
			$this->load->view('admin/elements/language_selection_view');
		?>
		<!-- End Language Selection -->
		
		<form class="form-horizontal" action="<?=site_url('admin/products/create')?>" method="post" id='main_form_submit'>
			<div class='form-group'>
	            <label class='control-label col-sm-2' for='active'>Active</label>
	            <div class='col-sm-9'>

	            <?php 
		            echo "<input name='active'  id='active' type='hidden'  value='N'/>";
		     	 		echo form_checkbox('active','Y','checked');

	            ?>
	            </div>
            </div>
            
            <div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Category	</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_dropdown('category_id',$cats,'','class="form-control"');
					?>
				 </div>
            </div>

            
			<div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Name</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_input('name',set_value('name'),'class="form-control"');
					?>
				 </div>
            </div>
            
            
            <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Images (1280x720)</label>
				 <div class='col-sm-9'>
					<p style="padding-bottom:10px;"></p>
		     	 	<?php	
		    			$this->load->view('admin/elements/fileupload_view.php',array('file'=>'product_image','id'=>'Product_image','value'=>'','type_file'=>'product','multiple'=>true));
		    		?>
				 </div>
            </div> 
            
            <div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Product made by Machines:</label>
				 <div class='col-sm-9'>
					 <?php
						 if(isset($machines)):
						 	if(!empty($machines)):
						 		foreach($machines as $k =>$v):
					  ?>
								<div class="col-md-4 machines" style='text-align:center;'>
									<p>
										<img src="<?=base_url()?>assets/upload/img/thumbnail/<?=$v->image?>" width=100  height=100/>
										
										
									</p>
									<p><?=$v->translations[0]->name?></p>
									<?php echo form_checkbox('machines[]',$v->id);?>
									
								</div>
					
					<?php
								endforeach; // machines
							endif; //end if empty machines
						endif; //end if isset machines
						?>
				 </div>
            </div>
            
             <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Short Description</label>
				 <div class='col-sm-9'>
					<textarea class="form-control" id="short_description" name="short_description" rows="6"></textarea>
				 </div>
            </div>
            
            <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Description</label>
				 <div class='col-sm-9'>
					<textarea class="form-control editor" id="description" name="description"></textarea>
				 </div>
            </div>
            
              <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Sort</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_input('sort','5','class="form-control" style="width:10%"');
						 echo form_hidden('position','main_top_banner');
					?>
				 </div>
            </div>
            
            <div class="form-group">
					 <label class="control-label col-sm-2" for="pwd">Page title:</label>
					 <div class='col-sm-9'>
						<textarea class="form-control" id="page_title" name="page_title">
						</textarea>
					 </div>
	            </div>
	            
	            <div class="form-group">
					 <label class="control-label col-sm-2" for="pwd">Page keywords:</label>
					 <div class='col-sm-9'>
						<textarea class="form-control" id="page_keywords" name="page_keywords">
						</textarea>
					 </div>
	            </div>
	            
	            <div class="form-group">
					 <label class="control-label col-sm-2" for="pwd">Page description:</label>
					 <div class='col-sm-9'>
						<textarea class="form-control" id="page_description" name="page_description">
						</textarea>
					 </div>
	            </div>

            
            <div class="form-group">
	            <label class="control-label col-sm-2" for="pwd"></label>        
		      <div class="col-sm-9">
		        <button type="submit" class="btn btn-primary cmd-save">Save</button>
		      </div>
	    	</div>
		</form>
	</div>
</div>