<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container">
	<div class="col-sm-3 col-md-3">
		<?php $this->load->view('admin/elements/left_menu_view');?>
	</div>
	<div class="col-right col-sm-9 col-md-9">
		<h3 class="text-capitalize"><?php echo $page_name ?></h3>
		<!-- Languages Selection -->
		<?php
			$this->load->view('admin/elements/language_selection_view');
		?>
		<!-- End Language Selection -->
		
		<form class="form-horizontal" action="<?=site_url('admin/products/edit/'.$item->id)?>" method="post" id='main_form_submit'>
			<div class='form-group'>
	            <label class='control-label col-sm-2' for='active'>Active</label>
	            <div class='col-sm-9'>

	            <?php 
		            echo "<input name='active'  id='active' type='hidden'  value='N'/>";
		     	 		echo form_checkbox('active','Y','checked');
		     	 		echo form_hidden('id',set_value('id',$item->id));

	            ?>
	            </div>
            </div>
            
            <div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Category	</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_dropdown('category_id',$cats,$item->category_id,'class="form-control"');
					?>
				 </div>
            </div>

            
			<div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Name</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_input('name',set_value('name',$item->translations[0]->name),'class="form-control"');
					?>
				 </div>
            </div>
            
            
            <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Images</label>
				 <div class='col-sm-9'>
					
		     	 	<?php	
		    			$this->load->view('admin/elements/fileupload_view.php',array('file'=>'product_image','id'=>'Product_image','value'=>'','type_file'=>'product','multiple'=>true));
		    		?>
		    		 <?php
			    		 
			    		 if(!empty($item->image)):
				    ?>
				    <div class="col-md-4">
					    <p>
						 	<a href="<?=base_url()?>assets/upload/product/<?=$item->image?>" target="blank"><img src="<?=base_url()?>assets/upload/product/thumbnail/<?=$item->image?>" style='border:1px solid #ddd'/></a>
						 <br><span><?=$item->image?></span><br /><a style='color:#f00'  href='<?=base_url()?>admin/upload/delete/product/<?=$item->image?>'>Delete</a>
						</p>
				    </div>
				    <?php
			    		 endif;
			    		 
						 if(!empty($item->images)):
						 	foreach($item->images as $k => $img):
					 ?>
						 <div class="col-md-4">
							 	<p>
								 	<a href="<?=base_url()?>assets/upload/product/<?=$img->image?>" target="blank"><img src="<?=base_url()?>assets/upload/product/thumbnail/<?=$img->image?>" style='border:1px solid #ddd'/></a>
								 <br><span><?=$img->image?></span><br /><a style='color:#f00'  href='<?=base_url()?>admin/upload/deletefile/product/<?=$img->image?>'>Delete</a></p>
								 
						 </div>

					 <?php
						 	endforeach;
						 endif;
						 ?>
				 </div>
            </div> 
            
            <div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Product made by Machines:</label>
				 <div class='col-sm-9'>
					 <?php
						 if(isset($machines)):
						 	if(!empty($machines)):
						 		foreach($machines as $k =>$v):
						 		$check = "";
						 		
						 			foreach($item->product_machine as $k => $m){
							 			if($v->id == $m->machine_id){
								 			$check = "checked";
								 			break;
							 			}
						 			}
					  ?>
								<div class="col-md-4 machines" style='text-align:center;'>
									<p>
										<img src="<?=base_url()?>assets/upload/img/thumbnail/<?=$v->image?>" width=100  height=100/>
										
										
									</p>
									<p><?=$v->translations[0]->name?></p>
									<?php echo form_checkbox('machines[]',$v->id,$check);?>
									
								</div>
					
					<?php
								endforeach; // machines
							endif; //end if empty machines
						endif; //end if isset machines
						?>
				 </div>
            </div>
            
             <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Short Description</label>
				 <div class='col-sm-9'>
					<textarea class="form-control" id="short_description" name="short_description" rows="6"><?=$item->translations[0]->short_description?></textarea>
				 </div>
            </div>
            
            <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Description</label>
				 <div class='col-sm-9'>
					<textarea class="form-control editor" id="description" name="description"><?=$item->translations[0]->description?></textarea>
				 </div>
            </div>
            
              <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Sort</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_input('sort','5','class="form-control" style="width:10%"');
						 echo form_hidden('position','main_top_banner');
					?>
				 </div>
            </div>
            
            <div class="form-group">
					 <label class="control-label col-sm-2" for="pwd">Page title:</label>
					 <div class='col-sm-9'>
						<textarea class="form-control" id="page_title" name="page_title">
						<?=$item->translations[0]->page_title?>
						</textarea>
					 </div>
	            </div>
	            
	            <div class="form-group">
					 <label class="control-label col-sm-2" for="pwd">Page keywords:</label>
					 <div class='col-sm-9'>
						<textarea class="form-control" id="page_keywords" name="page_keywords">
						<?=$item->translations[0]->page_keywords?>
						</textarea>
					 </div>
	            </div>
	            
	            <div class="form-group">
					 <label class="control-label col-sm-2" for="pwd">Page description:</label>
					 <div class='col-sm-9'>
						<textarea class="form-control" id="page_description" name="page_description">
						<?=$item->translations[0]->page_description?>
						</textarea>
					 </div>
	            </div>

            
            <div class="form-group">
	            <label class="control-label col-sm-2" for="pwd"></label>        
		      <div class="col-sm-9">
		        <button type="submit" class="btn btn-primary cmd-save">Save</button>
		      </div>
	    	</div>
		</form>
	</div>
</div>