<!-- Pay fee -->
<?php 
	echo form_open('admin/receipt/student/'.$data->id);
	/*Hidden Element*/
?>
<div class='box box-primary box-border'>
	<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>'Pay Dept','box_tool'=>true,'border'=>true))?>
	<div class='box-body'>
		<dl class="dl-horizontal">
            <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>    
                <dt><?=__('Full name',$this)?></dt>
                <dd><?=$data->name?></dd>
                
                <dt><?=__('Course',$this)?></dt>
                <dd><?=$data->course->name?></dd>
                
                <dt><?=__("Dept Amount",$this)?>:</dt>
                <dd><span class='currency'><?=$dept->dept?></span></dd>
                <dt><?=__("Pay Amount",$this)?></dt>
                <dd>
			       <div class="input-group">
		                <input type="text" name="pay_amount" class="form-control">
		                
		                <span class="input-group-addon">$</span>
		              </div>
                </dd>
                <dt>
                </dt>
                <dd>
                	<div class="form-group">
	                		<br>
	                		<button type="submit" class="btn btn-block btn-primary btn-flat" style="display: block;"><?=__("Pay Dept",$this)?></button>
	                </div>
                </dd>
            </div>
            <!-- end ./col -->
             <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>    
                
             </div>
             <!-- end ./col -->
        </dl>
	</div>
</div>
<?php
	echo form_hidden('student_id',$data->id);
	echo form_hidden('course_id',$data->course_id);
	echo form_hidden('total_value',$dept->dept);
	echo form_close();
	?>
