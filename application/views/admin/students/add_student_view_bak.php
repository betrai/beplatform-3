<!-- Add Students -->
<script type="text/javascript">
var courses =[];
var courses_info = [];
<?php foreach($courses as $c => $v):?>

courses[<?=$v->id?>] = [
	<?php foreach($v->classes as $key=>$value):?>
		{"id":<?=$value->id?>,"name":"<?=$value->name?>"},
	<?php endforeach; ?>
];

courses_info[<?=$v->id?>] = [{'tuition_fee':'<?=$v->tuition?>',
'chemicals_fee':'<?=$v->chemicals_fee?>',
'tool_fee':'<?=$v->tool_fee?>'}];
<?php endforeach; ?>
</script>
<?php echo form_open('')?>
	<!-- Student Information -->
		<div class='box box-primary box-border'>
			<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>'Student Information','box_tool'=>true,'border'=>true))?>
			
			<div class='box-body'>
				<div class='row'>
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						<!-- Today -->	
				          <div class="form-group">
				            <label for='d_of_consulting'><?=__('Day of Consulting',$this)?>:</label>
				
				            <div class="input-group date">
				              <div class="input-group-addon">
				                <i class="fa fa-calendar"></i>
				              </div>
				              <input type="text" class="form-control pull-right" id="doc">
				            </div>
				            <!-- /.input group -->
				          </div>
				        <!-- /.form group -->	
				        
						<!-- Name -->
						<div class='form-group'>
							<label><?=__("Full name",$this)?></label>
					        <input name="name" type="text" class="form-control" id="full_name" placeholder="<?=__("Full name",$this)?>">
						</div>
						
						<!-- DOB -->
						 <div class="form-group">
				            <label><?=__('Day of Birth',$this)?>:</label>
				
				            <div class="input-group date">
				              <div class="input-group-addon">
				                <i class="fa fa-calendar"></i>
				              </div>
				              <input type="text" class="form-control pull-right" id="dob">
				            </div>
				            <!-- /.input group -->
				          </div>
				          <!-- /.form group -->
				        <div class="form-group">
		                  <label><?=__("Courses",$this)?></label>
		                  <select class="form-control" id='courses' name='courses_id'>
			                  <option value=''><?=__("Choose a course",$this)?></option>
		                    <?php
			                    foreach($listing_courses as $k => $v):
			                    ?>
			                    <option value='<?=$k?>'><?=$v?></option>
			                <?php
				                endforeach;
				                ?>
		                  </select>
		                </div>
		                
		                <div class="form-group">
		                  <label><?=__("Class",$this)?></label>
		                  <select class="form-control" id='classes' disabled="disabled" name='classes_id'>
			                  <option value=''><?=__("Choose a course",$this)?></option>
		                    <?php
			                    foreach($listing_courses as $k => $v):
			                    ?>
			                    <option value='<?=$k?>'><?=$v?></option>
			                <?php
				                endforeach;
				                ?>
		                  </select>
		                </div>
		                
					</div>
					
					<!-- -->
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						<div class='row'>      
			                <div class='col-xs-12 col-sm-12 col-md-10 col-lg-10'> 
				                <div class='table-responsive text-right'>
						                <table class="table">
							              <tbody><tr>
							                <th style="width:50%"><?=__("Tuition",$this)?>:</th>
							                <td><p id="tuition">0</p></td>
							              </tr>
							              <tr>
							                <th><?=__("Chemicals Fee",$this)?></th>
							                <td><p id='chemical_fee'>0</p></td>
							              </tr>
							              <tr>
							                <th><?=__("Tools Fee",$this)?></th>
							                <td><p id='tool_fee'>0</p></td>
							              </tr>
							              <tr>
							                <th><?=__("Total",$this)?></th>
							                <td><p id='total_tuition_fee'>0</p></td>
							              </tr>
							             </tbody>
						            </table>
				              	</div>
				              	
				              	 <!-- Name -->
								<div class='form-group'>
									<label><?=__("Pay Tuition",$this)?></label>
							        <input name="pay_tuition" type="text" class="form-control text-right" id="tuition_student_pay" placeholder="<?=__("0",$this)?>">
							        <input name="indebtedness" type="hidden"  />
								</div>
								
								 <div class='table-responsive text-right'>
						            <table class="table">
							              <tbody>
								            <tr>
								                <th style="width:50%"><?=__("Debt",$this)?>:</th>
								                <td><p id="debt">0</p></tt>
							              	</tr>
							             </tbody>
						            </table>
				              	</div>

			                </div>
			                
			                <div class='col-xs-12 col-sm-12 col-md-2 col-lg-2'>
			                	 
			                </div>
						</div>
					</div>
				</div>	         
			</div>
			
			<!-- ./end box-body -->
		    <div class="box-footer">
			    <button type="submit" class="btn btn-primary"><?=__('Submit',$this)?></button>
			 </div>

		</div>
	<!-- ./end student information -->
	
<?php 
echo form_close();
?>