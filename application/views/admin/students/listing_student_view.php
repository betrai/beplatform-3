<?php
	$dept = array('Y','N');
	?>
<div class='row'>   
	<div class='col-md-12'>

	<div class="box">
	   	<!-- /.box-header -->
		<div class="box-header">
		  <h3 class="box-title"><?=__($page_name,$this)?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class='row'>
				<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3'>
				</div>
				<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3'>
				</div>
				<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3'>
					<!-- checkbox -->
		              <div class="form-group">
		                <label><?=__('Courses',$this)?></label>
		                <select class="form-control select2" id='select_course'>
			                <option value=""><?=__('Choose a course',$this)?></option>
		                 	<?php
			                 	$get_course_id = $_GET['course_id'];
			                 	foreach($listing_courses as $k => $c):
			                 	?>
			                 	<option value="<?=$k?>" <?php if($get_course_id == $k) echo 'selected';?>><?=$c?></option>
			                <?php
				                endforeach;
				                ?>
		                </select>
		              </div>
				</div>
				<div class='col-xs-12 col-sm-6 col-md-3 col-lg-3'>
					
		            <!-- checkbox -->
		              <div class="form-group">
		                <label><?=__('Dept',$this)?></label>
		                <select class="form-control select2" id='select_dept'>
		                 	<option value=""><?=__('Choose a dept status',$this)?></option>
		                 	<?php
			                 	$get_dept = $_GET['dept'];
			                 	foreach($dept as $k => $c):
			                 	?>
			                 		<option value="<?=$c?>" <?php if($get_dept == $c) echo 'selected';?>><?=$c?></option>
			                <?php
				                endforeach;
				                ?>
		                </select>
		              </div>
				</div>
			</div>
        <!-- /.form-group -->
		  <table id="data" class="table table-bordered table-striped table-hover">
		    <thead>
			    <tr>
			      <th><?=__("Students",$this)?></th>
			      <th class='text-center'><?=__("Courses",$this)?></th>
			      <th class='text-center'><?=__("Location",$this)?></th> 
			      <th class='text-center'><?=__("Pay Fee",$this)?></th>           
			    </tr>
		    </thead>
		    <tbody>
	        <?php
		        if(isset($data)):
		        	foreach($data as $k => $v):
	            ?>
				    <tr onmouseover="addShowClass(this)" onmouseout="addHideClass(this)">
				      <td class='sorting_1'>
					     <div class='pull-left' style='width:100%;'>
						     <?=anchor('admin/students/edit/'.$v->id,$v->name)?>
						     <span class='pull-right hide'>
						     	<a href='<?=base_url()?>admin/students/information/<?=$v->id?>' title='<?=__('Add Information',$this)?>'><i class='fa fa-address-card'></i></a>
						     	&nbsp;
						     	<a href='' title='<?=__('Deactive',$this)?>'><i class='fa <i class="fas fa-minus-circle'></i></a>
						     </span>
					     </div>	
					  </td>
				      <td class='text-right'>
					      <?=isset($v->course->name)?$v->course->name:""?>
				      </td>
				      <td class='text-right'>
					      <?php
						      if(isset($v->ward->title)):
						      ?>
					      <?=$v->address.' <br />'.$v->ward->title.' <br />'.$v->province->title?>
					      		<?php endif;?>
					  </td>
				      <!--<td class='text-center'><?=active($v->status,$v->id,array('class'=>'minimal-blue'))?></td>-->
				      <td class='text-center'>
					      <?php
						  	if($v->paid == 'N'):
						  ?>
						  <!--<div class='btn-group'>
							  <button type='button' class='btn btn-warning' title='<?=__("Pay fee",$this)?>' data-url="<?=base_url()?>admin/students/payfee/<?=$v->id?>" onclick="location.assign('<?=base_url()?>admin/students/payfee/<?=$v->id?>')">
							  <i class='fa fa-usd'></i>
							  </button>
						  </div>-->
						  <a href='<?=base_url()?>admin/students/payfee/<?=$v->id?>'>
							  <i class='fa fa-info'></i>
						  </a>
						  <!--<button type="button" class='btn btn-block btn-default btn-xs'><?=__("Pay fee",$this)?></button>-->
						  <?php
						      elseif($v->dept == 'Y'):
						  ?>
						  <a href='<?=base_url()?>admin/students/paydept/<?=$v->id?>'>
							  <i class='fa fa-money'></i>
						  </a>
						  <!--<div class='btn-group'>
							   <button type="button" class="btn btn-info btn-flat" data-url="<?=base_url()?>admin/students/paydept/<?=$v->id?>" onclick="location.assign('<?=base_url()?>admin/students/paydept/<?=$v->id?>')"><i class='fa fa-usd'></i></button>
						  </div>-->
    
						  <?php
							  else:
							  ?>
							<!-- <div class='btn-group'>
								   <button type="button" class="btn btn-success btn-flat"><i class="fa fa-check-square"></i></button>
							  </div>-->
								 <span class='text-green'> <i class='fa fa-check-circle'></i></span>
							  <?php
								  
							  endif;
						  ?>
				      </td>
				    </tr>
		    <?php
		        	endforeach;
		        else: 
		        ?>
		        <tr>
			        <td class="no-data text-center" colspan="">
				        <?=__('No data',$this)?>
			        </td>
		        </tr>
		    <?php
			    endif;
			    ?>
		    </tbody>
		    <tfoot>
		        <tr>
			      <th><?=__("Students",$this)?></th>
			      <th class='text-center'><?=__("Grand Day",$this)?></th>
			      <th class='text-center'><?=__("Location",$this)?></th>
			      <th class='text-center'><?=__("Pay Fee",$this)?></th>           
			    </tr>
		    </tfoot>
		  </table>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
	</div>
	</div>
</div>
<?php
	if(isset($_SESSION['alert'])):
		$alert = $_SESSION['alert'];
		$this->load->view('admin/elements/modules/message_box_view',array('modal_style'=>$alert['type'],'message'=>$alert['message'],'title'=>$alert['title']));	
		unset($_SESSION['alert']);	
	endif;
?>

<script type='text/javascript'>
	function addShowClass(element){
		//console.log(element.childNodes[0].nextSibling.childNodes[1].childNodes[2].className);
		element.childNodes[0].nextSibling.childNodes[1].childNodes[2].nextSibling.className = "pull-right show";
		return;
		//element.childNodes[0].childNodes[] = 'show';	
	}
	
	function addHideClass(element){
		element.childNodes[0].nextSibling.childNodes[1].childNodes[2].nextSibling.className = "hide";
		return;
	}
</script>
