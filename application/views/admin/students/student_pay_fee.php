<!-- Pay fee -->

<?php echo form_open('admin/receipt/student/'.$data->id);
	/*Hidden Element*/
	echo form_hidden('student_id',$data->id);
?>
<div class='box box-primary box-border'>
	<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>'Pay Fee','box_tool'=>true,'border'=>true))?>
	<div class='box-body'>
		<dl class="dl-horizontal">
            <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>    
                <dt><?=__('Full name',$this)?></dt>
                <dd><?=$data->name?></dd>
                
                <dt><?=__('Course',$this)?></dt>
                <dd><?=$data->course->name?></dd>
                
                <dt><?=__('Special case',$this)?></dt>
                <dd>
	                <label>
	                  <input id="deflation_checkbox" type="checkbox" class="minimal" value='Y' />
	                </label>
                </dd>
                
                <div class='deflation' style='display:none;'>
	                <dt><?=__('Deflation',$this)?></dt>
	                <dd>
		                <div class="form-group">
			                <select id="type_deflation" name="type_deflation" class="form-control select2 type_deflation" style="width: 100%;">
			                  <option selected="selected" value='percent'><?=__('%',$this)?></option>
			                  <option value='fixed'><?=__('fixed',$this)?></option>
			                 </select>
			              </div>
	                </dd>
	                
	                <dt><?=__('Deflation Amount',$this)?></dt>
	                <dd>
		                <div class="form-group">
		                  <input name='show_deflation_amount' id="deflation_amount" type="text" class="currency form-control" placeholder="<?=__("Enter amount",$this)?>">
	                	</div>
	                </dd>
	                	                
	                <dt><?=__('Reduce Chemicals',$this)?></dt>
	                <dd>
		                <div class="form-group">
			                <select id="type_reduce_chemicals" name="type_reduce_chemicals" class="form-control select2 type_reduce_chemicals" style="width: 100%;">
			                  <option selected="selected" value='percent'><?=__('%',$this)?></option>
			                  <option value='fixed'><?=__('fixed',$this)?></option>
			                 </select>
			              </div>
	                </dd>
	                
	                <dt><?=__('Reduce Chemicals Amount',$this)?></dt>
	                <dd>
		                <div class="form-group">
		                  <input name='show_reduce_chemicals_amount' id="reduce_chemicals_amount" type="text" class="currency form-control" placeholder="<?=__("Enter amount",$this)?>">
	                	</div>
	                </dd>
	                
	                <dt></dt>
	                <dd>
	                	<div class="form-group">
	                		<button type="button" class="btn btn-block btn-default" id="calculate"><?=__("Calculate",$this)?></button>
	                	</div>
	                </dd>


                </div>
            </div>
            <!-- end ./col -->
             <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>    
                
	                <dt><?=__('Tuition',$this)?>: </dt>
	                <dd class='currency text-right'><?=$data->course->tuition?></dd>
	                
	                <dt><span class='text-green'><?=__('Deflation Amount',$this)?></span><span class='label_deflation_amount' style='display:none;'></span></dt>
	                <dd id='show_deflation_amount' class='currency text-right'>0</dd>
	                
	                <dt><?=__('Chemicals Fee',$this)?>: </dt>
	                <dd class='currency text-right'><?=$data->course->chemicals_fee?></dd>
	                <dt><span class='text-green'><?=__('Reduce Chemicals Amount',$this)?></span><span class='label_reduce_chemicals_amount' style='display:none;'></span></dt>
	                <dd id='show_reduce_chemicals_amount' class='currency text-right'>0</dd>

	                
	                <dt><?=__('Tools Fee',$this)?>: </dt>
	                <dd class='currency text-right'><?=$data->course->tool_fee?></dd>
	                
	                <dd><hr /></dd>
	                
	                <dt><?=__('Total',$this)?></dt>
	                <dd class='currency text-right' id="total_amount"><?=$data->course->tuition + $data->course->chemicals_fee + $data->course->tool_fee?></dd>
	                
	                <dd><hr /></dd>
	                
	                <dt><?=__('Pay amount',$this)?></dt>
	                <dd class='text-right'>
	                	<input id='pay_amount' class='currency text-right'/>
	                </dd>
	                
	                <dt><?=__('Dept amount',$this)?></dt>
	                <dd class='currency text-right' id='dept_amount'>0</dd>
	                
	                <dt><dt>
	                <dd>
		                <div class="form-group">
	                		<br />
	                		<button type="submit" class="btn btn-block btn-primary btn-flat" style='display:none;'><?=__("Preview Invoice",$this)?></button>
		                </div>
	                </dd>
	                
             </div>
             <!-- end ./col -->
        </dl>
	</div>
</div>
<?php

	$total_amount = $data->course->tuition + $data->course->chemicals_fee + $data->course->tool_fee;
	echo form_hidden('pay_amount');
	echo form_hidden('total_fee_after_deflation');
	echo form_hidden('total_value',$total_amount);
	echo form_hidden('deflation_amount');
	echo form_hidden('course_id',$data->course->id);
	
	echo form_hidden('total_fee',$total_amount);
	echo form_hidden('items[1][name]',__("Tuition",$this));
	echo form_hidden('items[1][price]',$data->course->tuition);
	echo form_hidden('items[1][deflation_amount]');
	
	echo form_hidden('items[2][name]',__("Chemicals Fee",$this));
	echo form_hidden('items[2][price]',$data->course->chemicals_fee);
	
	echo form_hidden('items[2][reduce_chemical_amount]');
	
	echo form_hidden('items[3][name]',__("Tools Fee",$this));
	echo form_hidden('items[3][price]',$data->course->tool_fee);
	
	echo form_close();
	?>
