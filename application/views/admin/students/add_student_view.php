<?php echo form_open('',array('id'=>'add_form','data-toggle'=>'validator','role'=>'form'));
	
	/*Hidden Element*/
	echo form_hidden('name','');
?>
	<!-- Student Information -->
		<div class='box box-primary box-border'>
			<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>'Student Information','box_tool'=>true,'border'=>true))?>
			
			<div class='box-body'>
				<div class='row'>
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						<div class='form-group'>
							<label><?=__("Full name",$this)?></label>
					        <input name="name" value="<?=isset($data->name)?$data->name:""?>" type="text" class="form-control" id="full_name" placeholder="<?=__("Full name",$this)?>" required>
						</div>
						
						<div class="form-group">
		                  <label><?=__("Phone",$this)?></label>
		                  <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-phone"></i>
			                  </div>
			                  <input name='phone' type="text" class="form-control"
			                         data-inputmask="'mask': ['999-999-9999', '+099 99 99 999']" data-mask  value="<?=isset($data->phone)?$data->phone:""?>" required>
			                </div>
			                <!-- /.input group -->
		                </div>
				        
					</div>
					<!-- -->
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						
		                
		                <div class="form-group">
		                  <label><?=__("Courses",$this)?></label>
		                  <select class="form-control" id='courses' name='course_id' required>
			                  <option value=''><?=__("Choose a course",$this)?></option>
		                    <?php
			                    if(isset($data->course->id)){
						        	$course_id = $data->course->id;
					            }
			                    foreach($listing_courses as $k => $v):
			                    	if(isset($course_id) && ($course_id == $k)){
				                    	$selected = "selected";
			                    	}
			                    ?>
			                    <option value='<?=$k?>' <?=isset($selected)?$selected:""?>><?=$v?></option>
			                <?php
				                	unset($selected);
				                endforeach;
				                ?>
		                  </select>
		                </div>		                
		                <div class='form-group'>
			                <label>
			                  <input name='special_case' value='Y' type="checkbox" class="minimal-blue">
				                 <?=__("Special case",$this)?>
			                </label>
		                </div>
		                <div class='form-group'>
		                <?php 
			                
			                if(isset($data)):
			                	if($data->dept == 'Y'):
			                ?>
			                	<label>
				                  <input name='pay_dept' value='Y' type="checkbox" class="minimal-blue" checked>
					                 <?=__("Pay Dept",$this)?>
				                </label>
			                <?php
				                endif;
				            else:
				                ?>
				                <label>
				                  <input name='pay_fee' value='Y' type="checkbox" class="minimal-blue" checked>
					                 <?=__("Pay fee",$this)?>
				                </label>
				            <?php
				            endif; 
				            ?>
						</div>
					</div>
				</div>	         
			</div>
			
			<!-- ./end box-body -->
		    <div class="box-footer">
			    <button type="submit" class="btn btn-primary"><?=__('Submit',$this)?></button>
			 </div>

		</div>
	<!-- ./end student information -->
	
<?php
echo form_close();
?>