<?php echo form_open('');
	
	/*Hidden Element*/
	echo form_hidden('id',$data->id);
	echo form_hidden('student_information[id]',isset($data->student_information->id)?$data->student_information->id:'');
?>
	<!-- Student Information -->
		<div class='box box-primary box-border'>
			<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>__('Students Detail',$this),'box_tool'=>true,'border'=>true))?>
			
			<div class='box-body'>
				<div class='row'>
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						<div class='form-group'>
							<label><?=__("Full name",$this)?></label>
					        <input name="name" value="<?=isset($data->name)?$data->name:""?>" type="text" class="form-control" id="full_name" placeholder="<?=__("Full name",$this)?>">
						</div>
						
						<div class="form-group">
		                  <label><?=__("Phone",$this)?></label>
		                  <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-phone"></i>
			                  </div>
			                  <input name='phone' type="text" class="form-control"
			                         data-inputmask="'mask': ['999-999-9999', '+099 99 99 999']" data-mask  value="<?=isset($data->phone)?$data->phone:""?>" >
			                </div>
			                <!-- /.input group -->
		                </div>
		                
		                <div class="form-group">
		                  <label><?=__("Day of birth",$this)?></label>
		                  <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input name='student_information[dob]' type="text" id='dob' class="form-control"
			                          value="<?=isset($data->student_information->dob)?$data->student_information->dob:""?>" >
			                </div>
			                <!-- /.input group -->
		                </div>
		                
		                <div class="form-group">
		                  <label><?=__("Relative name",$this)?></label>
		                  <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-address-book"></i>
			                  </div>
			                  <input name='student_information[relative_name]' type="text" class="form-control"
			                         data-inputmask="'mask':<?=__('Relative name',$this)?>" data-mask  value="<?=isset($data->student_information->relative_name)?$data->student_information->relative_name:""?>" >
			                </div>
			                <!-- /.input group -->
		                </div>
		                
		                <div class="form-group">
		                  <label><?=__("Relative phone",$this)?></label>
		                  <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-phone"></i>
			                  </div>
			                  <input name='student_information[relative_phone]' type="text" class="form-control"
			                         data-inputmask="'mask': ['999-999-9999', '+099 99 99 999']" data-mask  value="<?=isset($data->student_information->relative_phone)?$data->student_information->relative_phone:""?>" >
			                </div>
			                <!-- /.input group -->
		                </div>
		                
		                <div class="form-group">
		                  <label><?=__("Province",$this)?></label>
		                  <select class="form-control select2" id='province' name='student_information[province_id]' style='width:100%;'>
			                  <option value=''><?=__("Choose a province",$this)?></option>
		                    <?php
			                    if(isset($data->student_information->province_id)){
						        	$province_id = $data->student_information->province_id;
					            }
			                    foreach($provinces as $k => $v):
			                    	if(isset($province_id) && ($province_id == $k)){
				                    	$selected = "selected";
			                    	}
			                    ?>
			                    <option value='<?=$k?>' <?=isset($selected)?$selected:""?>><?=$v?></option>
			                <?php
				                	unset($selected);
				                endforeach;
				                ?>
		                  </select>
		                </div>	
		                
		                <div class="form-group">
		                  <label><?=__("Wards",$this)?></label>
		                  <select class="form-control select2" id='provinces' name='student_information[ward_id]' style='width:100%;'>
			                  <option value=''><?=__("Choose a ward",$this)?></option>
		                    <?php
			                     if(isset($data->student_information->ward_id)){
						        	$ward_id = $data->student_information->ward_id;
					            }
			                    foreach($wards as $k => $v):
			                    	if(isset($ward_id) && ($ward_id == $k)){
				                    	$selected = "selected";
			                    	}
			                    ?>
			                    <option value='<?=$k?>' <?=isset($selected)?$selected:""?>><?=$v?></option>
			                <?php
				                	unset($selected);
				                endforeach;
				                ?>
		                  </select>
		                </div>	
				        
					</div>
					<!-- -->
					<div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
						
		                
		                <div class="form-group">
		                  <label><?=__("Courses",$this)?></label>
		                  <select class="form-control" id='courses' name='course_id'>
			                  <option value=''><?=__("Choose a course",$this)?></option>
		                    <?php
			                    if(isset($data->course_id)){
						        	$course_id = $data->course_id;
					            }
			                    foreach($courses as $k => $v):
			                    	if(isset($course_id) && ($course_id == $k)){
				                    	$selected = "selected";
			                    	}
			                    ?>
			                    <option value='<?=$k?>' <?=isset($selected)?$selected:""?>><?=$v?></option>
			                <?php
				                	unset($selected);
				                endforeach;
				                ?>
		                  </select>
		                </div>		                
		                <div class='form-group'>
			                <label>
			                  <input name='special_case' value='Y' type="checkbox" class="minimal-blue" <?=($data->special_case=='Y'?'checked':'')?>>
				                 <?=__("Special case",$this)?>
			                </label>
		                </div>
		                <div class='form-group'>
		                <?php 
			                
			                if(isset($data)):
			                	if($data->dept == 'Y'):
			                ?>
			                	<label>
				                  <input name='pay_dept' value='Y' type="checkbox" class="minimal-blue" checked>
					                 <?=__("Pay Dept",$this)?>
				                </label>
			                <?php	
				                endif;
				            else:
				                ?>
				                <label>
				                  <input name='pay_fee' value='Y' type="checkbox" class="minimal-blue" checked>
					                 <?=__("Pay fee",$this)?>
				                </label>
				            <?php
				            endif; 
				            ?>
						</div>
					</div>
				</div>	         
			</div>
			
			<!-- ./end box-body -->
		    <div class="box-footer">
			    <button type="submit" class="btn btn-primary"><?=__('Submit',$this)?></button>
			 </div>

		</div>
	<!-- ./end student information -->
	
<?php
echo form_close();
?>