<!-- Student Transfer to Head office -->
<div class='row'>
	<div class='col-xs-12 col-md-8 col-md-8'>
		<div class='box box-primary box-border'>
			<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>__('Transfer to head office',$this),'box_tool'=>true,'border'=>true))?>
			
			<div class='box-body'>
				<div class='row'>
					<div class='col-md-4'>
						<div class="form-group">
		                <select class="form-control select2" id='select_course' style='width:100%;'>
			                <option value=""><?=__('Choose a course',$this)?></option>
		                 	<?php
			                 	$get_course_id = $_GET['course_id'];
			                 	foreach($listing_courses as $k => $c):
			                 	?>
			                 	<option value="<?=$k?>" <?php if($get_course_id == $k) echo 'selected';?>><?=$c?></option>
			                <?php
				                endforeach;
				                ?>
		                </select>
		              </div>

					</div>
					<div class='col-md-4'></div>
					<div class='col-md-4'>
						<!-- checkbox -->
		            </div>
				</div>
				<div class='row'>
						<div class='col-md-12'>
							<table id="data" class="table table-bordered table-striped table-hover">
							    <thead>
								    <tr>
									  <th><a id='check_all'><?=__('Check all',$this)?></a></th>
								      <th><?=__("Students",$this)?></th>
								      <th><?=__("Course",$this)?></th>
								      <th><?=__("Amount Paid",$this)?></th>
								      <th><?=__("Dept",$this)?></th>           
								    </tr>
							    </thead>
							    <tbody>
								    <?php
									    if(!empty($data)):
										    foreach($data as $k=>$v):
										    	// Proccess 
										    	$pay_amount = 0;
											    foreach($v->receipt as $r => $pay){
											  	  $pay_amount += $pay->pay_amount;
										    	}
										    	
										    	$dept = 0;
										    	if(is_object($v->dept)){
											    	$dept = $v->dept->dept;
										    	}else{
											    	$dept = $v->dept;
										    	}
										    	// ./end proccess
										    ?>
										    <tr>
											    <td class='text-center'><?=form_checkbox($v->id,$v->id.'-'.$pay_amount.'-'.$dept,false,array('class'=>'minimal-red'))?></td>	
											    <td><?=$v->name?></td>
											    <td><?=$v->course->name?></td>
											    <td class='text-right'>
												    <span class='currency'>
												    <?=$pay_amount?>
												    </span>
												</td>
											    <td class='text-right'>
												    <span class='currency'><?=$dept?></span>
											    </td>
										    </tr>
										<?php
											endforeach;
										endif;
											?>
							    </tbody>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
	<!-- col 2 -->
	<div class='col-xs-12 col-md-4 col-md-4'>
		<div class='box box-primary box-border box-warning'>
			<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>__('Listing',$this),'box_tool'=>true,'border'=>true))?>
			<?php
				echo form_open('');
				echo form_hidden('student_ids');
				echo form_hidden('total_student');
				echo form_hidden('total_payamount');
				echo form_hidden('total_dept');
				
				?>
			<div class='box-body'>
				<dl>
	                <dt><?=__('Total choice student',$this)?></dt>
	                <dd><span id='show_total_student' class=''>0</span></dd>
	                <dt><?=__('Total money',$this)?></dt>
	                <dd><span id='show_total_payamount' class='currency'>0</span></dd>
	                <dt><?=__('Total dept',$this)?></dt>
	                <dd><span id='show_total_dept' class='currency'>0</span></dd>
              </dl>
              <button type="submit" class="btn btn-default btn-block btn-flat"><?=__('Transfer',$this)?></button>
              <?php
	             echo  form_close();
	              ?>
			</div>
	</div>
</div>