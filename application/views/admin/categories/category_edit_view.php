<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container">
	<div class="col-sm-3 col-md-3">
		<?php $this->load->view('admin/elements/left_menu_view');?>
	</div>
	<div class="col-right col-sm-9 col-md-9">
		<h3 class="text-capitalize"><?php echo $page_name ?></h3>
		<!-- Languages Selection -->
		<?php
			$this->load->view('admin/elements/language_selection_view');
		?>
		<!-- End Language Selection -->
		
		<form class="form-horizontal" action="<?=site_url('admin/categories/edit/'.$item->id)?>" method="post" id='main_form_submit'>
			<div class='form-group'>
	            <label class='control-label col-sm-2' for='active'>Active</label>
	            <div class='col-sm-9'>

	            <?php 
		            echo "<input name='active'  id='active' type='hidden'  value='N'/>";
		     	 	echo form_checkbox('active','Y',($item->active=='Y'?true:false));
		     	 	echo form_hidden('id',set_value('id',$item->id));

	            ?>
	            </div>
            </div>
            
			<div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Name</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_input('name',set_value('name',$item->translations[0]->name),'class="form-control"');
					?>
				 </div>
            </div>
            
            
            <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Images (1280x720)</label>
				 <div class='col-sm-9'>
					<?php
							 	if(!empty($item->image)):
							 ?>
							 <p style="padding-bottom:10px;">
							<img src='<?=base_url()?>assets/upload/img/thumbnail/<?=$item->image?>' />
						</p>
						<?php
								endif;
							?>
		     	 	<?php	
		    			$this->load->view('admin/elements/fileupload_view.php',array('file'=>'image','id'=>'Category_image','value'=>$item->image,'multiple'=>false));
		    		?>
				 </div>
            </div> 
            
             <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Short Description</label>
				 <div class='col-sm-9'>
					<textarea class="form-control" id="short_description" name="short_description"><?php echo $item->translations ? set_value('short_description',$item->translations[0]->short_description):""?></textarea>
				 </div>
            </div>
            
            <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Description</label>
				 <div class='col-sm-9'>
<textarea class="form-control editor" id="description" name="description"><?php echo $item->translations ? set_value('description',$item->translations[0]->description):""?></textarea>
				</div>
            </div>
            
              <div class='form-group'>
	            <label class="control-label col-sm-2" for="pwd">Sort</label>
				 <div class='col-sm-9'>
					<?php
						 echo form_input('sort','5','class="form-control" style="width:10%"');
					?>
				 </div>
             </div>
            
             <div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Page title:</label>
				 <div class='col-sm-9'>
					<textarea class="form-control" id="page_title" name="page_title"><?php echo $item->translations ? set_value('page_keywords',$item->translations[0]->page_title):""?>
					</textarea>
				 </div>
            </div>
            
            <div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Page keywords:</label>
				 <div class='col-sm-9'>
					<textarea class="form-control" id="page_keywords" name="page_keywords"><?php echo $item->translations ? set_value('page_keywords',$item->translations[0]->page_keywords):""?>
					</textarea>
				 </div>
            </div>
            
            <div class="form-group">
				 <label class="control-label col-sm-2" for="pwd">Page description:</label>
				 <div class='col-sm-9'>
					<textarea class="form-control" id="page_description" name="page_description"><?php echo $item->translations ? set_value('page_description',$item->translations[0]->page_description):""?>
					</textarea>
				 </div>
            </div>


            
            <div class="form-group">
	            <label class="control-label col-sm-2" for="pwd"></label>        
		      <div class="col-sm-9">
		        <button type="submit" class="btn btn-primary cmd-save">Save</button>
		      </div>
	    	</div>
		</form>
	</div>
</div>