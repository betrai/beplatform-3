<?=form_open('admin/processing/create/course/add',array('id'=>'add_form'));?>

<div class='row'>
	<div class='col-xs-12 col-md-12 col-lg-12'>
		<div class="box box-default">
		    <div class="box-header">
				<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>'Listing Students Submit by Academy','box_tool'=>true,'border'=>true))?>
		    </div>
		    <!-- /.box-header -->
		    
		    <div class="box-body">
			    <!-- /.form-group -->
		  <table id="data" class="table table-bordered table-striped table-hover">
		    <thead>
			    <tr>
				  <th width='10%'>STT</th>
			      <th><?=__("Students",$this)?></th>
			      <th><?=__("Amount",$this)?></th>
			      <th class='text-center'  width='10%'><?=__("Approve",$this)?></th>           
			    </tr>
		    </thead>
		    <tbody>
	        <?php
		        if(isset($data)):
		        $i = 1;
		        $total_amount = 0;
		        	foreach($data as $k => $v):
	            ?>
				    <tr>
					   <td class='text-center'>
						   <?=$i?>
					   </td>
				      <td class='sorting_1'>
					     <div class='pull-left' style='width:100%;'>
						     <?=anchor('admin/students/edit/'.$v->student->id,$v->student->name)?>
					     </div>	
					  </td>
				     <td class='text-right'>
					     <span class='currency'><?=$v->student->receipt->pay_amount?></span>
				     </td>
				      <td class='text-center'>
					      <div class='form-group'>
							   <?=form_checkbox($v->id,$v->id,false,array('class'=>'minimal-red'))?>
						   </div>
				      </td>
				    </tr>
		    <?php
			    		$i++;
			    		$total_amount += $v->student->receipt->pay_amount;
		        	endforeach;
		        else: 
		        ?>
		        <tr>
			        <td class="no-data text-center" colspan="">
				        <?=__('No data',$this)?>
			        </td>
		        </tr>
		    <?php
			    endif;
			    ?>
		    </tbody>
		    <tfoot>
			    <tr>
				    <td colspan='3' class='text-right'><?=__("Total",$this)?>: <strong><span class='currency'><?=$total_amount?></span></strong></td>
				    <td></td>
			    </tr>
		        <tr>
			      <th>STT</th>
			      <th><?=__("Students",$this)?></th>
			      <th>Amount</th>
			      <th class='text-center'><?=__("Approve",$this)?></th>           
			    </tr>
			    
		    </tfoot>
		  </table>
		<!-- /.box-body -->
		    </div>
		    <div class='row'>
			    <button type='submit'>Submit</button>
		    </div>
		</div>
	</div>
</div>
<?=form_close();?>