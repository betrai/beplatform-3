<div class='row'>   
	<div class='col-md-12'>

	<div class="box">
	   	<!-- /.box-header -->
		<div class="box-header">
		  <h3 class="box-title"><?=__($page_name,$this)?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table id="data" class="table table-bordered table-striped table-hover">
		    <thead>
			    <tr>
			      <th><?=__("Courses",$this)?></th>
			      <th class='text-center'><?=__("Grand Day",$this)?></th>
			      <th class='text-center'><?=__("Closing Ceremony Day",$this)?></th>
			      <th class='text-center'><?=__("Location",$this)?></th>
			      <th class='text-center'><?=__("Total student",$this)?></th>
			      <!--<th class='text-center'><?=__("Status",$this)?></th>-->
			      <th class='text-center'><?=__("Actions",$this)?></th>           
			    </tr>
		    </thead>
		    <tbody>
	        <?php
	            foreach($data as $k => $v):
	            ?>
		    <tr>
		      <td><?=$v->name?></td>
		      <td class='text-right'><?=date('d-m-Y',strtotime($v->grand_opening_day))?></td>
		      		      <td class='text-right'><?=date('d-m-Y',strtotime($v->closing_ceremony_day))?></td>

		      <td class='text-right'><?=$location[$v->location]?></td>
		      <td class='text-right'><?=$v->students?></td>
		      <!--<td class='text-center'><?=active($v->active,$v->id,'minimal-red')?></td>-->
			  <td class='text-right'>
			  	<!-- Trash item -->
			  	<a href='#'><i class='fa fa-trash'></i></a> 
			  	&nbsp;
			  	<a href='#'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
			  </td> 
		    </tr>
		    <?php
		        endforeach;
		        ?>
		    </tbody>
		    <tfoot>
		        <tr>
		          <th><?=__("Courses",$this)?></th>
			      <th class='text-center'><?=__("Grand Day",$this)?></th>
			      <th class='text-center'><?=__("Closing Ceremony Day",$this)?></th>
			      <th class='text-center'><?=__("Location",$this)?></th>
			      <th class='text-center'><?=__("Total student",$this)?></th>  
			      <!--<th class='text-center'><?=__("Status",$this)?></th>-->
			      <th class='text-center'><?=__("Actions",$this)?></th>  
		        </tr>
		    </tfoot>
		  </table>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
	</div>
       

<?php 
	$this->load->view('admin/courses/add_course_view');
?>
	</div>
</div>