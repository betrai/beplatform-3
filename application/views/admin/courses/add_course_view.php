<?=form_open('admin/processing/create/course/add',array('id'=>'add_form'));?>

<div class="box box-default">
    <div class="box-header">
		<?php $this->load->view('admin/elements/ui/box_header',array('box_title'=>'Add Courses','box_tool'=>true,'border'=>true))?>
    </div>
    <!-- /.box-header -->
    
    <div class="box-body">
	    
	    <?php echo validation_errors(); ?>
	    
	    <div class='row'>
		    <div class='col-md-6'>
			    <!-- text input -->
		        <div class="form-group">
		          <label><?=__("Course name",$this)?></label>
		          <input name="name" type="text" class="form-control" placeholder="<?=__("Enter",$this)?>" required>
		        </div>
		        
		        <!-- -->
		        <div class="form-group">
			        <label><?=__('Location',$this)?></label>
			        <select name='location' class="form-control select2" style="width: 100%;" required>
			          <?php
				          foreach($location as $k=>$l):
				          ?>
				        <option value="<?=$k?>"><?=$l?></option>
				      <?php
					      endforeach;
					      ?>
			        </select>
			    </div>	
			    
			    <div class="form-group">
			        <label><?=__("Grand Day",$this)?></label>
			
			        <div class="input-group date">
			          <div class="input-group-addon">
			            <i class="fa fa-calendar"></i>
			          </div>
			          <input name="grand_opening_day" type="text" class="form-control pull-right datepicker" id="datepicker" required>
			        </div>
			        <!-- /.input group -->
			    </div>
			    
			    <div class="form-group">
			         <label><?=__("Closing Ceremony Day",$this)?></label>
			
			        <div class="input-group date">
			          <div class="input-group-addon">
			            <i class="fa fa-calendar"></i>
			          </div>
			          <input name="closing_ceremony_day" type="text" class="form-control pull-right datepicker" id="closing_ceremony_day" required>
			        </div>
			        <!-- /.input group -->

		         </div>

		    </div>
		    
		    <div class='col-md-6'>
			    <div class="form-group">
		          <label><?=__("Tuition",$this)?></label>
		          <input name="tuition" type="text" class="form-control" value="15.000.000" placeholder="<?=__("",$this)?>">
		         </div>
		         
		         <div class="form-group">
			         <label><?=__("Chemicals Fee",$this)?></label>
					 <input name="chemicals_fee" type="text" class="form-control" value="3.000.000" placeholder="<?=__("3.000.000",$this)?>">
		         </div>
		         
		          <div class="form-group">
			         <label><?=__("Tool Fee",$this)?></label>
					 <input name="tool_fee" type="text" class="form-control" value="3.000.000" placeholder="<?=__("3.000.000",$this)?>">
		         </div>
		         
		          <div class="form-group">
			         <label><?=__("Teacher",$this)?></label>
			         <select name='teacher_id' class="form-control select2" style="width: 100%;" required>
					 <?php
				         foreach($teachers as $k=>$v):
				         ?>
				         <option value='<?=$k?>'><?=$v?></option>
				    <?php
					    endforeach;
					    ?>
			         </select>
		         </div>

		    </div>
		    
		    
	    </div>
	    <!-- ./end row -->
	</div>
	<!-- ./end box-body -->
    <div class="box-footer">
	    <button type="submit" class="btn btn-primary"><?=__('Submit',$this)?></button>
	 </div>
</div>
<?php
	/*Hidden Input*/
	echo '<input name="relation_model[name]" type="hidden" value="classes" />';
?>
<?=form_close()?>
