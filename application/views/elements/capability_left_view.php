<?php
	
	$uri = $this->uri->segment(2);?>
<div class="my-blog-col clearfix">
    <!-- archives start -->
    <div class="my-Archives">
    	<div class="my-sidebar-title">
    		<h4<?=$uri=='departments'?" class='brand_color'":""?>>
	    		<!--<i class="fa fa-map-signs"></i>--> 
	    		<a href="<?=base_url()?>capability/departments" class='brand_color'><span><?=__('Departments',$this)?></span></a>
	    	</h4>
    	</div>
    	<ul class="my-list">
	    	<?php
		    	foreach($items['departments'] as $k => $v):
		    ?>
			<li><?=$v->translations[0]->name?></li>
			<?php
				endforeach;
			?>
		</ul>
		
		<div class="my-sidebar-title">
    		<h4<?=$uri=='machines'?" class='brand_color'":""?>><!--<i class="fa icon-gears"></i>-->
    			<a href="<?=base_url()?>capability/machines"  class='brand_color'><span><?=__('List of Machines',$this)?></span></a>
	    	</h4>
    	</div>
    	<ul class="my-list">
	    	<?php
		    	foreach($items['machines'] as $k => $v):
		    ?>
			<li><span style='text-transform:initial;'><?=$v->translations[0]->name?></span></li>
			<?php
				endforeach;
			?>
		</ul>
    </div>
</div>	