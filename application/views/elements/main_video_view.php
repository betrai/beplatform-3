<?php
	if(!empty($video)):
	?>
	<!-- Video start -->
	<section class="my-video-field parallax my-layer-black" data-stellar-background-ratio="0.3">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="my-video-col text-center">
						<h2><?=$video->translations[0]->title?></h2>
						<p><?=$video->translations[0]->description?></p>
						<a class="popup-youtube" href="<?=base_url()?>assets/upload/video/<?=$video->file?>"><i class="fa fa-play-circle" aria-hidden="true"></i></a>
					</div>	
				</div>
			</div>
		</div>
	</section>
	<!-- Video end -->
<?php
	endif;
	?>
