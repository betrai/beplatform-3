<?php
	$uri = $this->uri->segment(2);
?>
<div class="my-blog-col clearfix">
    <!-- archives start -->
    <div class="my-Archives">
    	<div class="my-sidebar-title">
    		<h4<?=$uri=='departments'?" class='brand_color'":""?>><i class="fa fa-folder-o"></i> <?=__('Categories',$this)?></h4>
    	</div>
    	<ul class="my-list">
	    	<?php
		    	foreach($cats as $k => $v):
		    	?>
			<li><a href="<?=base_url().'news/'.$v->translations[0]->slug?>"  ><?=$v->translations[0]->name?></a></li>
			<?php
				endforeach;
				?>
		</ul>
		
    </div>
</div>	