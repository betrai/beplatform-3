<!-- Funfact Start-->
	<section class="my-funfact-field">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="my-funfact-col">
						<span class="icon-lightbulb"></span>
                        <div class="start-count">7</div>
                        <p>license of Unigraphic NX 8.5</p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="my-funfact-col">
						<span class="icon-layers"></span>
                        <div class="start-count">450+</div>
                        <p><?=__('Projects',$this)?></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="my-funfact-col">
						<span class="icon-genius"></span>
                        <div class="start-count">1600+</div>
                        <p><?=__('Working Days',$this)?></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="my-funfact-col">
						<span class="icon-target"></span>
                        <div class="start-count">200+</div>
                        <p><?=__('Clients',$this)?></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Funfact end-->