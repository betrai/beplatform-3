<!-- Recent Project Start-->
	<section class="my-project-field">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="my-sector-topp">
						<span class="icon-toolbox"></span>
		                <h2>OUR <span>BUSINESS</span></h2>
		                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy that our many<br> nibh euis mod tincidunt ut laoreet dolore.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
		            <div class="owl-carousel-grid-three gallery-slider">
		                <div class="item">
		                    <div class="thumb">
			                    <a href="#"><img class="img-responsive my-img-fluided" src="images/resource/business1.jpg" alt=""></a>
			                    <div class="layer">
			                      <a class="lightbox-image" href="images/resource/business1.jpg" data-fancybox-group="gallery" title="Gallery Photos"><i class="fa fa-plus" aria-hidden="true"></i></a>
			                    </div>
		                    </div>
		                </div>
		                <div class="item">
		                    <div class="thumb">
			                    <a href="#"><img class="img-responsive my-img-fluided" src="images/resource/business2.jpg" alt=""></a>
			                    <div class="layer">
			                      <a class="lightbox-image" href="images/resource/business2.jpg" data-fancybox-group="gallery" title="Gallery Photos"><i class="fa fa-plus" aria-hidden="true"></i></a>
			                    </div>
		                    </div>
		                </div>
		                <div class="item">
		                    <div class="thumb">
			                    <a href="#"><img class="img-responsive my-img-fluided" src="images/resource/business3.jpg" alt=""></a>
			                    <div class="layer">
			                      <a class="lightbox-image" href="images/resource/business3.jpg" data-fancybox-group="gallery" title="Gallery Photos"><i class="fa fa-plus" aria-hidden="true"></i></a>
			                    </div>
		                    </div>
		                </div>
		                <div class="item">
		                    <div class="thumb">
			                    <a href="#"><img class="img-responsive my-img-fluided" src="images/resource/business1.jpg" alt=""></a>
			                    <div class="layer">
			                      <a class="lightbox-image" href="images/resource/business1.jpg" data-fancybox-group="gallery" title="Gallery Photos"><i class="fa fa-plus" aria-hidden="true"></i></a>
			                    </div>
		                    </div>
		                </div>
		                <div class="item">
		                    <div class="thumb">
			                    <a href="#"><img class="img-responsive my-img-fluided" src="images/resource/business2.jpg" alt=""></a>
			                    <div class="layer">
			                      <a class="lightbox-image" href="images/resource/business2.jpg" data-fancybox-group="gallery" title="Gallery Photos"><i class="fa fa-plus" aria-hidden="true"></i></a>
			                    </div>
		                    </div>
		                </div>
		            </div>
				</div>
			</div>
		</div>
	</section>
	<!-- Recent Project end-->