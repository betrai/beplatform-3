
	<!-- About Section Start-->
    <section class="my-about-field">
        <div class="container">
            <div class="row">
            	<div class="col-md-12 text-center">
	            	<div class="my-big-title">
	            		<h1><?=$capability->translations[0]->teaser?></h1>
	            		<p><?=$capability->translations[0]->content?></p>
	            	</div>
            	</div>
            </div>
            <div class="row">
	            <?php
		            $length = count($departments);
		            
		            $col = 'col-md-4 col-lg-4';
		            
		            if($length < 3){
			            $col= 'col-md-6 col-lg-6';
		            }
		            
		            foreach($departments as $k => $v):
		        ?>
	            <div class="col-xs-12 col-sm-6 <?=$col?>">
	                <div class="my-about-col">
	                    <div class="my-about-col-content">
	                    	<div class="my-about-col-title">
	                    		<span class="<?=$v->icon?>"></span>
		                  		<h4><?=$v->translations[0]->name?></h4>
	                    	</div>
		                  	<p><?=$v->translations[0]->content?> </p>
	                    </div>
	                </div>
	            </div>
	            <?php
		            endforeach;
		        ?>
            </div>   
            <div class="row">
	            <?php foreach($departments as $k => $v):?>
                <div class="col-xs-12 col-sm-6 <?=$col?>">
                    <div class="about-img-column">
                        <img class="img-responsive my-img-fluided" src="<?=base_url()?>assets/upload/img/<?=$v->image?>" alt="<?=$v->translations[0]->name?>">
                        <div class="layer">
                            <div class="img-col-content">
                              <h3 class="title"><?=$v->translations[0]->name?></h3>
                              <p><?=$v->translations[0]->content?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                	endforeach;
                ?>
                
            </div>
        </div>
    </section>
	<!-- About Section end-->