<!-- Team Start-->
	<section class="my-team-field">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="my-sector-topp">
						<span class="icon-strategy"></span>
		                <h2>Our <span>Expert</span></h2>
		                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy that our many<br> nibh euis mod tincidunt ut laoreet dolore.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4">
	                <div class="my-team-col">
		                <div class="thumb">
		                    <img class="img-responsive my-img-fluided" src="images/resource/team1.jpg" alt="">
		                    <div class="layer"></div>
		                    <div class="thumb-content">
			                    <h4 class="title text-uppercase">ALLAN DONALD</h4>
			                    <h5 class="sub-title efinance-color">Ceo / Employee manger</h5>
		                    </div>
		                    <div class="icon">
			                    <i class="icon-plus fa fa-plus"></i>
			                    <ul class="team-icons">
			                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
			                    </ul>
		                    </div>
		                </div>
		                <div class="content">
		                  <p>Commodi dolor dolore asperiores! Nihil pariatur quis ducimus sequi illum eum, inventore recusandae that obcaecati ipsam, tempora repellendus consequuntur eius sed deserunt voluptatum omnis nulla culpa that quidem magni facilis asperiores ullam.</p>
		                  <a class="efinance-color" href="#">read more <i class="lnr lnr-arrow-right" aria-hidden="true"></i></a>
		                </div>
	                </div>
	            </div>
	            <div class="col-xs-12 col-sm-6 col-md-4">
	                <div class="my-team-col">
		                <div class="thumb">
		                    <img class="img-responsive my-img-fluided" src="images/resource/team2.jpg" alt="">
		                    <div class="layer"></div>
		                    <div class="thumb-content">
			                    <h4 class="title text-uppercase">janifar lorece</h4>
			                    <h5 class="sub-title efinance-color">Ceo / Employee manger</h5>
		                    </div>
		                    <div class="icon">
			                    <i class="icon-plus fa fa-plus"></i>
			                    <ul class="team-icons">
			                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
			                    </ul>
		                    </div>
		                </div>
		                <div class="content">
		                  <p>Commodi dolor dolore asperiores! Nihil pariatur quis ducimus sequi illum eum, inventore recusandae that obcaecati ipsam, tempora repellendus consequuntur eius sed deserunt voluptatum omnis nulla culpa that quidem magni facilis asperiores ullam.</p>
		                  <a class="efinance-color" href="#">read more <i class="lnr lnr-arrow-right" aria-hidden="true"></i></a>
		                </div>
	                </div>
	            </div>
	            <div class="col-xs-12 col-sm-6 col-md-4">
	                <div class="my-team-col">
		                <div class="thumb">
		                    <img class="img-responsive my-img-fluided" src="images/resource/team3.jpg" alt="">
		                    <div class="layer"></div>
		                    <div class="thumb-content">
			                    <h4 class="title text-uppercase">janifar lorece</h4>
			                    <h5 class="sub-title efinance-color">Ceo / Employee manger</h5>
		                    </div>
		                    <div class="icon">
			                    <i class="icon-plus fa fa-plus"></i>
			                    <ul class="team-icons">
			                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
			                        <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
			                    </ul>
		                    </div>
		                </div>
		                <div class="content">
		                  <p>Commodi dolor dolore asperiores! Nihil pariatur quis ducimus sequi illum eum, inventore recusandae that obcaecati ipsam, tempora repellendus consequuntur eius sed deserunt voluptatum omnis nulla culpa that quidem magni facilis asperiores ullam.</p>
		                  <a class="efinance-color" href="#">read more <i class="lnr lnr-arrow-right" aria-hidden="true"></i></a>
		                </div>
	                </div>
	            </div>
			</div>
		</div>
	</section>
	<!-- Team end-->