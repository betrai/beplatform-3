<!-- Testimonials Start-->
    <section class="my-testimonial-field">
        <div class="container">
	        <div class="row">
	            <div class="col-sm-12 col-md-8 col-md-offset-2">
	            	<div class="testimonial-carousel">
		            	<?php
			            	foreach($testimonials as $k=>$v):
			            ?>
			            	
	            		<div class="item">
				            <div class="text-center">
				                <span class="icon icon-chat"></span>
				                <p><?=$v->translations[0]->description?></p>            
				                <h5><?=$v->name?></h5>
				                <i class="fa fa-quote-right" aria-hidden="true"></i>
				                <!--<h6>Edge Theme</h6>-->
				            </div>
	            		</div>
	            		<?php
		            		endforeach;
		            	?>
	            	</div>
	            </div>
	        </div>
        </div>
    </section>
    <!-- Testimonials End -->