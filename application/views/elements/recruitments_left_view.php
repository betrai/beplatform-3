<?php
	
	$uri = $this->uri->segment(2);?>
<div class="my-blog-col clearfix">
    <!-- archives start -->
    <div class="my-Archives">
    	<div class="my-sidebar-title">
    		<h4<?=$uri=='departments'?" class='brand_color'":""?>><i class="fa icon-toolbox"></i> <?=__('Job list',$this)?></h4>
    	</div>
    	<ul class="my-list">
	    	<?php
		    	foreach($items as $k => $v):
		    	?>
			<li><a href="#<?=$v->translations[0]->slug?>"  ><?=$v->translations[0]->name?></a></li>
			<?php
				endforeach;
				?>
		</ul>
		
    </div>
</div>	