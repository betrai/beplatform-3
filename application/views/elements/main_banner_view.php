<?php
	
	if(!empty($main_banners)):
	?>
	<!-- Main Slider Start -->
	<section class="my-main-slider">
		<div class="container-fluid">
		    <!-- Slider Rebolution Start -->
            <div class="rev_slider_wrapper fullscreen-container" data-alias="concept1" style="background-color:#000000;padding:0px;">
                <!-- START REVOLUTION SLIDER 5.1.1RC fullscreen mode -->
                <div id="rev_slider1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.1.1RC">
                    <ul>
	                    <?php
		                    $i= 1;
		                	foreach($main_banners as $k => $banner) :  
		                	 
		                ?>
		                <!-- SLIDE 1 -->
                        <li data-index="rs-<?=$i?>" data-transition="fade" data-slotamount="default" data-easein="Power2.easeInOut" data-easeout="default" data-masterspeed="2000" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""  data-param10="" data-thumb="<?=base_url()?>assets/upload/img/thumbnail/<?=$banner->translations[0]->image?>" data-rotate="0" data-saveperformance="off" data-title="<?=$banner->translations[0]->title?>" data-description="" data-param1="">
                            <!-- MAIN IMAGE -->
                            <img src="<?=base_url()?>assets/upload/img/<?=$banner->translations[0]->image?>" alt="" data-bgposition="center 20%" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg img-responsive" data-duration="30000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140" >
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-shape tp-shapewrapper"
	                            id="rs-1-layer-1" 
	                            data-x="['center','center','center','center']" 
	                            data-hoffset="['0','0','0','0']" 
	                            data-y="['middle','middle','middle','middle']" 
	                            data-voffset="['0','0','0','0']" 
	                            data-width="full" 
	                            data-height="full" data-whitespace="nowrap" 
	                            data-transform_idle="o:1;" 
	                            data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;" 
	                            data-transform_out="opacity:0;s:1000;s:1000;" 
	                            data-start="0" 
	                            data-basealign="slide" 
	                            data-responsive_offset="off" 
	                            data-responsive="off" style="z-index: 5;background-color:rgba(0, 0, 0, 0.7);border-color:rgba(0, 0, 0, 0);">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-resizeme font-Montserrat"
	                            id="rs-1-layer-3" 
	                            data-x="['center','center','center','center']" 
	                            data-hoffset="['0','0','0','0']" 
	                            data-y="['middle','middle','middle','middle']" 
	                            data-voffset="['-65','-65','-45','-50']" 
	                            data-fontsize="['48','48','48','48']" 
	                            data-lineheight="['54','54','54','54']" 
	                            data-width="['none','none','none','400']" 
	                            data-height="none" 
	                            data-whitespace="['nowrap','nowrap','nowrap','normal']" 
	                            data-transform_idle="o:1;"
	                            data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:1500;e:Power3.easeOut;" 
	                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
	                            data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;"
	                            data-start="1700" 
	                            data-splitin="none" 
	                            data-splitout="none" 
	                            data-responsive_offset="on"
	                            style="z-index: 7; white-space: nowrap;text-align:center;color: #ffffff; font-weight: 700;text-transform:uppercase;"><?=$banner->translations[0]->title?>                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-resizeme"
	                            id="rs-1-layer-2" 
	                            data-x="['center','center','center','center']" 
	                            data-hoffset="['0','0','0','0']" 
	                            data-y="['middle','middle','middle','middle']" 
	                            data-voffset="['10','10','20','80']" 
	                            data-fontsize="['18','18','15','15']" 
	                            data-lineheight="['30','30','25','25']" 
	                            data-width="['850','640','640','360']"  
	                            data-height="none" 
	                            data-whitespace="normal" 
	                            data-transform_idle="o:1;"
	                            data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:1500;e:Power3.easeOut;" 
	                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
	                            data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;"
	                            data-start="1500"
	                            data-splitin="none" 
	                            data-splitout="none" 
	                            data-responsive_offset="on" style="z-index: 6;white-space: nowrap; letter-spacing: 1px; text-align: center;color: #ffffff;font-weight: 400;"><?=$banner->translations[0]->description?>
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption tp-resizeme"
	                            id="rs-1-layer-4" 
	                            data-x="['center','center','center','center']" 
	                            data-hoffset="['0','0','0','0']" 
	                            data-y="['middle','middle','middle','middle']" 
	                            data-voffset="['80','80','80','170']" 
	                            data-whitespace="nowrap" 
	                            data-transform_idle="o:1;" 
	                            data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:1500;e:Power3.easeOut;" 
	                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
	                            data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;"
	                            data-start="1900" 
	                            data-responsive_offset="on" style="z-index: 8;">
	                            <a class="btn my-btn-mid text-uppercase bdrs_null" href="<?=$banner->link?>">More detail</a>
                            </div>
                        </li>
		                <?php
			                $i++;
			            	endforeach;
			            ?>
                        
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- Slider Rebolution End -->
		</div>
	</section>
	<!-- Main Slider end -->
	
	<?php endif;?>