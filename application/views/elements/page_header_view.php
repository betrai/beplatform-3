<style>
	.my-inner-heading-field {
    background: url("<?=base_url()?>assets/upload/img/<?=$item->top_banner?>");
}
</style>
<section class="my-inner-heading-field my-layer-black">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="my-inner-heading-col text-center">
						<h1><?=$item->translations[0]->name?></h1>
						<ul>
							<li><a href="<?=base_url()?>"><?=__('Home',$this)?></a></li>
							<li><a href="">/</a></li>
							<li><a href="" class="active"><?=$item->translations[0]->name?></a></li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
	</section>