<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Public_Controller {

	
	public function __construct(){
		parent::__construct();
		
	}
	
	function index(){
		
	}
	
	function about(){
		$this->load->model('page_model');
		
		$item = $this->page_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->get(array('page_name'=>'about_us'));
		
		$this->data['item'] = $item;
		//pr($item);
		$this->render('default/pages/about_us_view');
	}
	
	function contact(){
		$this->load->model('page_model');
		
		$form = $this->input->post();
		
				
		$item = $this->page_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->get(array('page_name'=>'contact_us'));
		
		//$this->data['email_sent'] = true;
		
		if(!empty($form)){
			if($this->sendFormMail($form,$item,$this->data['Settings'])){
				$this->data['email_sent'] = true;	
			}
		}

		
		$this->data['item'] = $item;
	
		$this->data['before_body'] .= $this->_addMap($item);
		
		$this->data['lang'] = $this->lang->load('contact',strtolower($this->data['current_lang']['name']));
		
		$this->render('default/pages/contact_us_view');
	}

	
	function _addMap($item){
		
		
		$script_map =  '<script type="text/javascript">
    $(window).load(function() {
        $("#map-canvas").googleMap({
            zoom:16, // Initial zoom level (optional)
            //coords: ['.$item->longitude.' ,'.$item->latitude.'], // Map center (optional)
            longitude: '.$item->longitude.',
            latitdue: '.$item->latitude.',
            type: "ROADMAP", // Map type (optional)
            address: "347 Le Duc Tho Street, Ward 17, Go Vap District, Ho Chi Minh", // Postale Address
            infoWindow: {
                content: \'<p style="text-align:center;"><strong>Canal Saint-Martin,</strong><br> Paris, France</p>\'
            }
        });
        // Marker 2
        $("#map-canvas").addMarker({
            coords: ['.$item->longitude.' ,'.$item->latitude.']
        });
    });
	</script>';
	return $script_map;
	}
	
	function sendFormMail($form,$item,$config){
		
		$this->load->library('email');
		
		$config['protocol'] = 'smtp';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";
		
		$config['smtp_host'] = $config['smtp_host'];
		$config['smtp_user'] = $config['smtp_user'];
		$config['smtp_pass'] = $config['smtp_password'];
		$config['smtp_port'] = '25';
		
		
		$this->email->from('raijuichi@live.com', 'CNS AMURA');
		$this->email->to($item->receive_email);
		$this->email->cc($item->cc_email);
		//$this->email->bcc('them@their-example.com');
		
		$message = "
			Dear CNS Amura Team,
			
			We have new contact from website with information below:
			========================================================
			Contact Name: ".$form['form_name']." 
			Contact Email: ".$form['form_email']."
			Contact Phone: ".$form['form_phone']."
			Contact Subject: ".$form['form_subject']."
			
			Message: 
			".$form['form_message']."
			=========================================================
		";
		
		$this->email->subject(_('New Contact From Website',$this));
		$this->email->message($message);
		
		return $this->email->send();
	}
}