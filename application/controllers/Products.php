<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Public_Controller {

	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('product_model');
		$this->load->model('category_model');
		$this->load->model('page_model');
		
		$this->data['css_for_layout'] = '
		<link rel="stylesheet" href="'.base_url().'assets/css/prettyPhoto.css">';
		
		$this->data['before_body'] .= '
		<!-- The basic File Video plugin -->
				<script src="'.base_url().'assets/js/jquery.video-extend.js"></script>
				<script src="'.base_url().'assets/js/jquery.prettyPhoto.js"></script>
				
				';
		$this->data['before_body'] .= "
		<script>
		$(function(){
					//$('#switcher').themeswitcher();
		            $(\"a[rel^='prettyPhoto']\").prettyPhoto({
			            social_tools:'',
			            default_width: 1024,
						default_height: 600,
						theme: 'dark_rounded',
		            });
				});
			</script>";
		

	}
	
	function index($slug = ""){
		
		$item = $this->page_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->get(array('page_name'=>'products'));
		
		$this->data['item'] = $item;

		$this->render('default/products/product_view');
	}
	
	function category($slug = ""){
		
		$item = $this->page_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->get(array('page_name'=>'products'));
		$this->data['item'] = $item;
		
		$productmachines = $this->page_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->get(array('page_name'=>'product_machines'));
		$this->data['productmachines'] = $productmachines;
		
		
		$items = $this->category_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->get_all(array('active'=>'Y'));
		$this->data['items'] = $items;
		if(empty($slug)){
			$slug = $items[0]->slug;
		}
		
		/*Get product by category id*/
		$category = $this->category_model->get(array('slug'=>$slug));
		
		if(!empty($category)){
			$products = $this->product_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->with_images()->get_all(array('category_id'=>$category->id,'active'=>'Y'));
			$this->data['products'] = $products;
		}
		
		
		$this->load->model('machine_model');
		$machines = $this->machine_model->with_translations('where:`language_slug`=\''.$this->current_lang.'\'')->get_all(array('active'=>'Y'));
		
		$this->data['machines'] = $machines;
		

		$this->render('default/products/category_view');
	}
}