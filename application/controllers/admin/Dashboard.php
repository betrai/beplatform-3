<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	function __construct(){
		parent::__construct();
		
		if(!$this->ion_auth->in_group('admin'))
        {
            $this->session->set_flashdata('message','You are not allowed to visit the Pages page');
            redirect('admin/user/login','refresh');
        }
		
		$this->data['script_for_layout'] .= assets('plugins/iCheck/icheck.min.js');
		
		$this->data['script_for_layout'] .= assets('chart.js/Chart.js');
		$this->data['script_for_layout'] .= assets('js/dashboard.js');
		
		$this->lang->load('dashboard',strtolower($this->data['current_lang']['name']));
		
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('widget_model');
		$this->load->helper('widget_helper');
		//pr($this);
		
		$this->data['widgets'] = $this->widget_model->getAll($this->ion_auth->get_user_id());
		
		//Load Widgets Language
		foreach($this->data['widgets'] as $k=>$v){
			$this->lang->load(strtolower($v->name).'_widget',strtolower($this->data['current_lang']['name']));

		}
		
		$this->render('admin/dashboard/dashboard_view');
	}
	
	public function settings(){
		
	}
	
	public function widget_statistic($course_id = ""){
		$this->load->model('course_model');
		
		if($course_id == ""){
			$this->db->select_max("course_id");
			$this->db->from('courses');
			$query = $this->db->get('');
			
			$course = $query->result();
		}
		
		
	}
}
